var packaging = {};
var products = [];
var decorations = new Set();

const PackagingScreen = new (function () {
  this.start = () => {
    $("#canvas").css({
      position: "absolute",
      top: 0,
      left: 0,
      "z-index": -999,
      width: "100%",
      minHeight: "100vh",
      height: "fit-content",
      padding: "4.5rem 0",
      backgroundColor: "bisque",
    });
    $("#canvas").addClass("d-flex flex-column");
    this.renderPackaging();
    HampersCart.renderCart(0);
    if (packaging.hasOwnProperty("size")) HampersCart.renderPackaging();
    if (toArray(products).length > 0) HampersCart.renderProducts();
    if (decorations.size > 0) HampersCart.renderDecoration();
  };

  this.renderPackaging = () => {
    $("#canvas").empty();
    $("#canvas").append(`<div class="container-fluid bg-light py-3 mb-4">  
        <div class="container position-relative">        
        <h1>Step 1</h1>
        <h3>Choose your packaging size.</h3>
        <div class="btn-group" style="position: absolute; bottom: 0; right: 0;">\
            <div onclick="PackagingScreen.next()" class="btn btn-primary">Next</div>
          </div>
        </div>        
    </div>
    <div class="container">
        <div id="packagingContainer" class="row g-3"></div>
    </div>`);
    for (var i = 0; i < this.packagingChoices.length; i++) {
      const packaging = this.packagingChoices[i];
      $("#packagingContainer").append(`
        <div class="col-md-4 col-6">
            <div class="text-center">
              <img src="${packaging.img}" class="rounded-circle w-75 mx-auto" alt="${packaging.size}">
              <div class="card-body d-flex flex-column align-items-center">
                <div class="fs-5 fw-bold mb-1">${packaging.size}</div>
                <div class="badge badge-lg rounded-pill bg-secondary">Capacity: ${packaging.intSize} unit</div>
                <div id="add-${i}" onclick="PackagingScreen.addToCart(${i})" class="button btn btn-primary mx-auto mt-2"><i class="fa fa-shopping-cart"></i> Add to cart</div>
              </div>
            </div>                    
        </div>
        `);
    }
  };

  this.addToCart = (idx) => {
    packaging = this.packagingChoices[idx];
    $("#packaging").text("");
    $("#packaging").append(`<div class="d-flex">
        <img src="${packaging.img}" style="width: 3rem; height: 3rem; border-radius: 50%;">
        <div class="d-flex flex-column flex-fill align-items-start ms-2">
            <div>${packaging.size}</div>
            <div class="badge rounded-pill bg-secondary my-1">Capacity: ${packaging.intSize} unit</div>
        </div>
    </div>`);
    $("#total-content").text(`0/${packaging.intSize}`);
    new Toast({
      message: `${packaging.size} packaging added to cart.`,
      type: "success",
    });
  };

  this.next = () => {
    if (packaging.hasOwnProperty("size")) ProductScreen.start();
    else {
      swal("Not yet!", "Please choose a packaging size first.", "warning");
    }
  };

  this.packagingChoices = [
    {
      size: "Smol",
      intSize: 10,
      img: "https://images-na.ssl-images-amazon.com/images/I/61O8evWG8UL._SL1000_.jpg",
    },
    {
      size: "Modest",
      intSize: 35,
      img: "https://www.kmart.com.au/wcsstore/Kmart/images/ncatalog/f/5/42788195-1-f.jpg",
    },
    {
      size: "Huge",
      intSize: 50,
      img: "https://millsdisplay.co.nz/wp-content/uploads/2019/04/Medium-Poly-Wicker-Basket-Natural.jpg",
    },
  ];
})();

const HampersCart = new (function () {
  this.renderCart = (state) => {
    var next = "";
    var nextOnClick = "";
    switch (state) {
      case 0:
        next = "Continue to Products";
        nextOnClick = "PackagingScreen.next()";
        break;
      case 1:
        next = "Continue to Decorations";
        nextOnClick = "ProductScreen.next()";
        break;
      case 2:
        next = "Checkout";
        nextOnClick = "DecorationScreen.next()";
        break;
    }
    $("#canvas").append(
      `<div style="position: fixed; bottom: 1rem; right: 1rem; z-index: 10; "><button id="cartBtn" class="btn btn-primary btn-lg rounded-circle" style="width: 60px; height: 60px;" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"><i class="fa fa-shopping-cart"></i></button>
            </div>
            <div class="offcanvas offcanvas-end" data-bs-scroll="true"  data-bs-backdrop="false" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel" style="padding-top: 5rem;">
              <div class="offcanvas-header">
                <h5 id="offcanvasRightLabel">Your Hampers</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
              <div id="packaging" class="offcanvas-header border-top border-bottom">
                <div class="d-flex">
                  <div style="width: 3rem; height: 3rem; border-radius: 50%; background-color: burlywood;"></div>
                  <div class="d-flex flex-column flex-fill align-items-start ms-2">
                      <div>No packaging size chosen.</div>
                      <div class="badge rounded-pill bg-secondary my-1">Capacity: 0 unit</div>
                  </div>
                </div>
              </div>
              <div id="cart" class="offcanvas-body"></div>
              <div class="offcanvas-footer d-flex flex-column justify-content-between p-3 pb-4 border-top" >
                <div id="decorations" class="d-flex align-items-center mb-3">
                  <div class="d-flex align-items-center">
                    <div style="width: 1rem; height: 1rem; border-radius: 50%; background-color: rosybrown; margin-right: .5rem"></div> No decorations chosen
                  </div>                  
                </div>
                <div class="d-flex justify-content-between"><h6>Total items:</h6><span id="total-item">0</span></div>  
                <div class="d-flex justify-content-between mb-2"><h6>Hampers capacity:</h6><span id="total-content">0/0</span></div>            
                <div onclick="${nextOnClick}" class="btn btn-primary">${next}</div>
              </div>
            </div>`
    );
  };

  this.renderPackaging = () => {
    $("#packaging").text("");
    $("#packaging").append(`<div class="d-flex">
        <img src="${packaging.img}" style="width: 3rem; height: 3rem; border-radius: 50%;">
        <div class="d-flex flex-column flex-fill align-items-start ms-2">
            <div>${packaging.size}</div>
            <div class="badge rounded-pill bg-secondary my-1">Capacity: ${packaging.intSize} unit</div>
        </div>
    </div>`);
    $("#total-content").text(`0/${packaging.intSize}`);
  };

  this.renderProducts = () => {
    $("#cart").text("");
    const pArray = toArray(products);
    for (let i = 0; i < pArray.length; i++) {
      const product = ProductScreen.products.find((x) => x.id === pArray[i].id);
      $("#cart").append(`
                <div class="d-flex align-items-start w-100 pb-3 mb-3 border-bottom">
                    <img src="${product.img}" class="rounded-circle me-2" style="height: 6rem;" alt="${product.name}">
                    <div class="d-flex flex-column flex-fill align-items-start">
                        <div>${product.name}</div>
                        <div class="badge rounded-pill bg-secondary my-1">${product.unit} unit</div>
                        <input id="qty-${product.id}" type="number" onchange="ProductScreen.updateQty(${product.id})" class="form-control" min="0" max="${product.stock}" value="${pArray[i].qty}">
                    </div>
                    <div class="btn btn-danger btn-sm ms-5" onclick="ProductScreen.removeFromCart(${product.id})"><i class="fa fa-trash"></i></div>
                </div>
            `);
    }
    $("#total-item").text(ProductScreen.calculateItems());
    $("#total-content").text(`${ProductScreen.content}/${ProductScreen.CAPACITY}`);
  };

  this.renderDecoration = () => {
    DecorationScreen.renderCart();
  };
})();

$(document).ready(function () {
  PackagingScreen.start();
});
