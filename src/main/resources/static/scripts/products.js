const ProductScreen = new (function () {
  this.products = null;
  this.content = 0;
  this.CAPACITY = 0;

  this.start = () => {
    this.CAPACITY = packaging.intSize;
    this.fetchAllProducts();
  };

  this.fetchAllProducts = () => {
    $.ajax({
      url: "/api/stock-management/available"
    }).done(function (data) {
      ProductScreen.products = data;
      ProductScreen.renderProducts();
      ProductScreen.renderFilter();

      HampersCart.renderCart(1);
      if (packaging.hasOwnProperty("size")) HampersCart.renderPackaging();
      if (toArray(products).length > 0) HampersCart.renderProducts();
      if (decorations.size > 0) HampersCart.renderDecoration();
    });
    // $.getJSON("products.json", function (json) {
    //   ProductScreen.products = json;
    //   ProductScreen.renderProducts();
    //   ProductScreen.renderFilter();
    //
    //   HampersCart.renderCart(1);
    //   if (packaging.hasOwnProperty("size")) HampersCart.renderPackaging();
    //   if (toArray(products).length > 0) HampersCart.renderProducts();
    //   if (decorations.size > 0) HampersCart.renderDecoration();
    // });
  };

  this.renderProducts = () => {
    $("#canvas").text("");
    $("#canvas").append(`<div class="container-fluid bg-light py-3 mb-4">  
        <div class="container position-relative">        
          <h1>Step 2</h1>
          <h3>Choose your products.</h3>
          <div class="btn-group" style="position: absolute; bottom: 0; right: 0;">
            <div onclick="PackagingScreen.start()" class="btn btn-outline-secondary">Previous</div>
            <div onclick="ProductScreen.next()" class="btn btn-primary">Next</div>
          </div>
        </div>        
      </div>`);
    $("#canvas").append(
      `<div class="container">        
        <div id="filters" class="row mb-5 g-2 d-flex justify-content-end"></div>
        <div id="productContainer" class="row g-3"></div>
      </div>`
    );
    this.rerenderProduct(this.products);
  };

  this.rerenderProduct = (filteredProducts) => {
    $("#productContainer").text("");
    for (let i = 0; i < filteredProducts.length; i++) {
      const product = filteredProducts[i];
      $("#productContainer").append(`
        <div class="col-lg-3 col-md-4 col-6">
            <div class="text-center">
              <img src="${product.img}" class="rounded-circle w-75 mx-auto" alt="${product.name}">
              <div class="card-body d-flex flex-column align-items-center">
                <div class="fs-5 fw-bold mb-1">${product.name}</div>
                <div class="badge rounded-pill bg-secondary">${product.unit} unit</div>
                <div id="add-${product.id}" onclick="ProductScreen.addToCart(${product.id})" class="button btn btn-primary mx-auto mt-2"><i class="fa fa-shopping-cart"></i> Add to cart</div>
                <div style="font-size:.8em; color: rosybrown">Available: ${product.stock}pcs</div>
              </div>
            </div>                    
        </div>
      `);
    }
  };

  this.renderFilter = () => {
    $("#filters").append(`
    <div class="btn-group col-4">
      <input type="radio" class="btn-check" name="settings" id="filterBtn" autocomplete="off" checked>
      <label class="btn btn-outline-secondary" for="filterBtn"  data-bs-toggle="collapse" data-bs-target="#filterSetting" aria-expanded="false" aria-controls="filterSetting"><i class="fa fa-filter me-2"></i>Filter</label>

      <input type="radio" class="btn-check" name="settings" id="sortBtn" autocomplete="off">
      <label class="btn btn-outline-secondary" for="sortBtn" data-bs-toggle="collapse" data-bs-target="#sortSetting" aria-expanded="false" aria-controls="sortSetting"><i class="fa fa-sort me-2"></i>Sort</label>
    </div>
    <div class="collapse" id="filterSetting">
      <div class="card card-body">
        <div class="d-flex align-items-center col-8">
          <div class="me-2">Filter:</div>
          <input id="filter-min" type="number" min="0" max="100" class="form-control" placeholder="unit min: 10">
          <span class="mx-2">-</span>
          <input id="filter-max" type="number" min="0" max="100" class="form-control" placeholder="unit max: 100">
          <div class="btn btn-outline-secondary btn-sm text-nowrap ms-2" onclick="ProductScreen.applyFilter()"><i class="fa fa-filter me-2"></i>Apply Filter</div>
          <div class="btn btn-outline-danger btn-sm text-nowrap ms-2" onclick="ProductScreen.removeFilter()"><i class="fa fa-minus-circle me-2"></i>Remove Filter</div>
        </div>
      </div>
    </div>
    <div class="collapse" id="sortSetting">
      <div class="card card-body">
        <div class="d-flex align-items-center col-8">
        <div class="me-2">Sort:</div>
        <select id="sortBy" class="form-select me-2" aria-label="sortBy">
          <option disabled selected>Sort by</option>
          <option value="1">Product Name</option>
          <option value="2">Unit</option>
        </select>
        <select id="orderBy" class="form-select" aria-label="order">
          <option disabled selected>Ordering</option>
          <option value="1">Asc</option>
          <option value="2">Desc</option>
        </select>
        <div class="btn btn-outline-secondary btn-sm text-nowrap ms-2" onclick="ProductScreen.applySort()"><i class="fa fa-sort me-2"></i>Apply Sort</div>
        <div class="btn btn-outline-danger btn-sm text-nowrap ms-2" onclick="ProductScreen.removeSort()"><i class="fa fa-minus-circle me-2"></i>Remove Sort</div>
      </div>
    </div>
  </div>      
    `);
  };

  this.rerenderCart = () => {
    $("#cart").empty();
    const pArray = toArray(products);
    for (let i = 0; i < pArray.length; i++) {
      const product = this.products.find((x) => x.id === pArray[i].id);
      $("#cart").append(`
            <div class="d-flex align-items-start w-100 pb-3 mb-3 border-bottom">
                <img src="${product.img}" class="rounded-circle me-2" style="height: 6rem;" alt="${product.name}">
                <div class="d-flex flex-column flex-fill align-items-start">
                    <div>${product.name}</div>
                    <div class="badge rounded-pill bg-secondary my-1">${product.unit} unit</div>
                    <input id="qty-${product.id}" type="number" onchange="ProductScreen.updateQty(${product.id})" class="form-control" min="0" max="${product.stock}" value="${pArray[i].qty}">
                </div>
                <div class="btn btn-danger btn-sm ms-5" onclick="ProductScreen.removeFromCart(${product.id})"><i class="fa fa-trash"></i></div>
            </div>
        `);
    }
    $("#total-item").text(this.calculateItems());
    $("#total-content").text(`${this.content}/${this.CAPACITY}`);
  };

  this.addToCart = (id) => {
    var qty = products[id] || 0;
    if (this.validateCart(id, qty + 1)) this.rerenderCart();
    const product = this.products.find((x) => x.id === id);
    new Toast({
      message: `${product.name} added to cart.`,
      type: "success",
    });
  };

  this.updateQty = (id) => {
    var newQty = parseInt($(`#qty-${id}`).val());
    if (newQty === 0) this.removeFromCart(id);
    else if (this.validateCart(id, newQty)) this.rerenderCart();
  };

  this.removeFromCart = (idx) => {
    delete products[idx];
    this.content = this.calculateContents(toArray(products));
    this.rerenderCart();
  };

  this.validateCart = (id, qty) => {
    const product = this.products.find((x) => x.id === id);
    const tempProducts = toArray(products).filter((x) => x.id !== id);
    const tempContents = this.calculateContents(tempProducts);
    if (tempContents + product.unit * qty <= this.CAPACITY) {
      products = { ...products, [product.id]: qty };
      this.content = this.calculateContents(toArray(products));
      return true; // cart is valid
    }
    swal("Sorry!", "Your packaging doesn't have enough capacity.", "warning");
    return false; // cart invalid
  };

  this.calculateContents = (tempProducts) => {
    var contents = 0;
    for (let i = 0; i < tempProducts.length; i++) {
      const product = this.products.find((x) => x.id === tempProducts[i].id);
      contents += product.unit * tempProducts[i].qty;
    }
    return contents;
  };

  this.calculateItems = () => {
    var item = 0;
    toArray(products).map((product) => (item += product.qty));
    return item;
  };

  this.applyFilter = () => {
    const max = parseInt($("#filter-max").val()) || 100;
    const min = parseInt($("#filter-min").val()) || 0;
    const filteredProducts = this.products.filter((x) => x.unit >= min && x.unit <= max);
    this.rerenderProduct(filteredProducts);
  };

  this.applySort = () => {
    var sortOrder = $("#sortBy").val() + $("#orderBy").val();
    if (!$("#sortBy").val()) {
      swal("Oops!", "Choose the sort by criteria and try again.", "warning");
      return;
    } else if (!$("#orderBy").val()) {
      swal("Oops!", "Choose the sort ordering and try again.", "warning");
      return;
    }
    var sortedProducts = [...this.products];
    switch (sortOrder) {
      case "11":
        sortedProducts.sort((a, b) => a.name.localeCompare(b.name));
        break;
      case "12":
        sortedProducts.sort((a, b) => b.name.localeCompare(a.name));
        break;
      case "21":
        sortedProducts.sort((a, b) => a.unit - b.unit);
        break;
      case "22":
        sortedProducts.sort((a, b) => b.unit - a.unit);
        break;
    }
    this.rerenderProduct(sortedProducts);
  };

  this.removeSort = () => {
    this.rerenderProduct(this.products);
  };

  this.removeFilter = () => {
    this.rerenderProduct(this.products);
  };

  this.next = () => {
    if (toArray(products).length > 0) DecorationScreen.start();
    else {
      swal("Not yet!", "Please choose at least one product first.", "warning");
    }
  };
})();

function toArray(obj) {
  return Object.entries(obj).map((e) => ({ id: parseInt(e[0]), qty: e[1] }));
}
