var userRoles = "";
var username = "";
var orderData = [];
var state = "All Transaction";
var stat = {
    'Menunggu Konfirmasi' : 'menunggu',
    'Diproses' : 'menunggu',
    'Selesai'    : 'selesai',
    'Dibatalkan'   : 'dibatalkan'
};
var total = "";

function loadAllData(){
    if (state === "All Transaction") {
        let url = "/api/history/";
        if (userRoles === "USER") {url = url + "u/" + username;}
        $.ajax({
            type : 'GET',
            url : url,
            success : function (data) {
                orderData = data;
                if (orderData.length > 0) {
                    loop(orderData);
                    total = orderData.length;
                    document.getElementById('total').innerHTML = total;
                    if (userRoles === "ADMIN") {
                        rate();
                    }
                }
                else {
                    $('#search-nav').hide();
                    $("#table-card").css("box-shadow", "none");
                    $('#table').hide();
                    $("#table-card").append("<h3>No orders yet</h3>")
                }
            }
        });
    } else {
        loop(orderData);
    }
}

function loop(data) {
    $("#table").empty();
    $("#table").append(`
        <thead class="secondary-text">
        <tr>
            <th scope="col">DATE</th>
            <th scope="col">ORDER ID</th>
            <th scope="col">USERNAME</th>
            <th scope="col">STATUS</th>
            <th scope="col">#</th>
        </tr>
        </thead>
        <tbody id="tbody">
    `);
    for (let i = 0; i < data.length; i++) {
        const order = orderData[i];
        let date = new Date(order.createdAt).toUTCString();
        let details = "/history/" + order.orderId;
        date = date.split(' ').slice(0, 5).join(' ');
        $("#tbody").append(`
            <tr>
                <th>${date}</th>
                <td scope="row">${order.orderId}</td>
                <td>${order.pembeli}</td>
                <td><div class="status badge rounded-pill my-1" data-color="${order.status}">${order.status}</div></td>
                <td>
                    <a href="${details}">
                        details
                    </a>
                </td>
            </tr>
        `)
    };
    $("#table").append('</tbody>');

    let dc;
    let th;
    $(".status").each(function(index){
        th = $(this);
        dc = $(this).attr('data-color');
        $.each(stat, function(name, value){
            if(dc === name){
                th.addClass(value);
            }
        });
    });
}

function filter() {
    state = $('.form-select').find(":selected").text();
    if (state === "Last 30 Days") {
        const now = new Date();
        const lastMonth = new Date();
        lastMonth.setDate(now.getDate()-30);
        orderData = orderData.filter(function (obj) {
            let date = new Date(obj.createdAt);
            return (lastMonth.getTime() <= date.getTime()) && (date.getTime() <= now.getTime());
        });
        loop(orderData);
    } else {
        loadAllData();
    }
}

function rate() {
    const now = new Date();
    const last = new Date();
    const lastlast = new Date();

    last.setDate(now.getDate()-30);
    const thisMonth = orderData.filter(function (obj) {
        let date = new Date(obj.createdAt);
        return (last.getTime() <= date.getTime()) && (date.getTime() <= now.getTime());
    });

    lastlast.setDate(last.getDate()-30);
    const lastMonth = orderData.filter(function (obj) {
        let date = new Date(obj.createdAt);
        return (lastlast.getTime() <= date.getTime()) && (date.getTime() <= last.getTime());
    });
    let percentage = 0;
    if (lastMonth.length !== 0) {
        percentage = thisMonth.length * 100/lastMonth.length;
    }
    return percentage;
}

$( document ).ready(function() {
    $.ajax("/api/user").done(function( data ) {
        userRoles = data.authorities[0].authority;
    }).always(function () {
        loadAllData();
    });

    $("#search").keyup(function(){
        let input = $("#search").val();
        if (state === "All Transaction") {
            $.ajax({
                url : "/api/history/" + input,
                success : function (data) {
                    const order = data;
                    $("#tbody").empty();
                    if (input.length == 0) {
                        loadAllData();
                    } else {
                        let date = new Date(order.createdAt).toUTCString();
                        date = date.split(' ').slice(0, 5).join(' ');
                        $("#table").empty();
                        $("#table").append(`
                        <thead class="secondary-text">
                        <tr>
                            <th scope="col">DATE</th>
                            <th scope="col">ORDER ID</th>
                            <th scope="col">USERNAME</th>
                            <th scope="col">STATUS</th>
                            <th scope="col">#</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        `);
                        let details = "/history/" + order.orderId;
                        $("#tbody").append(`
                        <tr>
                            <th>${date}</th>
                            <td scope="row">${order.orderId}</td>
                            <td>${order.pembeli}</td>
                            <td><div class="status badge rounded-pill my-1">${order.status}</div></td>
                            <td>
                                <a href="${details}">
                                    details
                                </a>
                            </td>
                        </tr>
                        `);
                        $("#table").append('</tbody>');
                        let dc;
                        let th;
                        $(".status").each(function(index){
                            th = $(this);
                            dc = $(this).attr('data-color');
                            $.each(stat, function(name, value){
                                if(dc === name){
                                    th.addClass(value);
                                }
                            });
                        });
                    }
                },
                error: function(){
                    $("#table").empty();
                    $("#table").append('<p>Order with id no ' + input + ' not found</p>');
                }
            })
        } else {
            if (input.length == 0) {
                loadAllData();
            } else {
                let bool = false;
                for (let i = 0; i < orderData.length; i++) {
                    if (orderData[i].orderId == input) {
                        bool = true;
                        $.ajax({
                            url : "/api/history/" + input,
                            success : function (data) {
                                const order = data;
                                $("#tbody").empty();
                                if (input.length == 0) {
                                    loadAllData();
                                } else {
                                    let date = new Date(order.createdAt).toUTCString();
                                    date = date.split(' ').slice(0, 5).join(' ');
                                    $("#table").empty();
                                    $("#table").append(`
                                        <thead class="secondary-text">
                                        <tr>
                                            <th scope="col">DATE</th>
                                            <th scope="col">ORDER ID</th>
                                            <th scope="col">USERNAME</th>
                                            <th scope="col">STATUS</th>
                                            <th scope="col">#</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody">
                                    `);
                                    let details = "/history/" + order.orderId;
                                    $("#tbody").append(`
                                        <tr>
                                            <th>${date}</th>
                                            <td scope="row">${order.orderId}</td>
                                            <td>${order.pembeli}</td>
                                            <td><div class="status badge rounded-pill my-1">${order.status}</div></td>
                                            <td>
                                                <a href="${details}">
                                                    details
                                                </a>
                                            </td>
                                        </tr>
                                    `);
                                    $("#table").append('</tbody>');
                                    let dc;
                                    let th;
                                    $(".status").each(function(index){
                                        th = $(this);
                                        dc = $(this).attr('data-color');
                                        $.each(stat, function(name, value){
                                            if(dc === name){
                                                th.addClass(value);
                                            }
                                        });
                                    });
                                }
                            },
                            error: function(){
                                $("#table").empty();
                                $("#table").append('<p>Order with id no ' + input + ' not found</p>');
                            }
                        });
                        break;
                    }
                }
                if (!bool) {
                    $("#table").empty();
                    $("#table").append('<p>Order with id no ' + input + ' not found</p>');
                }
            }
        }
    });
});