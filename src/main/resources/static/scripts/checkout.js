const CheckoutScreen = new (function () {
  this.start = () => {
    this.renderOrderSummary();
    this.renderPackaging();
    if (decorations.size > 0) this.renderDecoration();
    this.renderProducts();
    this.renderCheckout();
  };

  this.renderOrderSummary = () => {
    $("#canvas").text("");
    $("#canvas").append(`<div class="container-fluid bg-light py-3 mb-4">  
        <div class="container position-relative">        
          <h1>Step 4</h1>
          <h3>Confirm your order.</h3>
          <div class="btn-group" style="position: absolute; bottom: 0; right: 0;">
            <div onclick="DecorationScreen.start()" class="btn btn-outline-secondary">Previous</div>
          </div>
        </div>        
      </div>
      <div class="container">
        <div id="checkoutContainer" class="row g-3 align-items-start"></div>
      </div>`);
  };

  this.renderPackaging = () => {
    $("#checkoutContainer").append(`
        <div id="packaging-decor" class="col-md-6">
        <div class="row pb-4 border-bottom border-light">
            <h5>Packaging</h5>
            <div class="d-flex align-items-center">
            <img src="${packaging.img}" class="rounded-circle me-4" style="width: 8rem; height: 8rem;" alt="${packaging.size}">
            <div class="card-body d-flex flex-column align-items-start">
                <div class="fs-5 fw-bold mb-1">${packaging.size}</div>
                <div class="badge badge-lg rounded-pill bg-secondary">${packaging.intSize} unit</div>
                <div onclick="PackagingScreen.start()" class="button btn btn-primary btn-sm mt-2"><i class="fa fa-edit me-2"></i> Edit</div>
            </div>
            </div>                    
        </div>
        </div>
    `);
  };

  this.renderDecoration = () => {
    $("#packaging-decor").append(`
        <div id="decor-final" class="row mt-3"> 
        <h5>Decorations</h5>                 
        </div>
      `);
    const a = Array.from(decorations);
    for (let i = 0; i < a.length; i++) {
      const decor = DecorationScreen.decorationOption.find((x) => x.jenisDekorasi === a[i]);
      $("#decor-final").append(`
        <div class="d-flex align-items-center w-100 mb-3">
          <img src="${decor.img}" class="rounded-circle me-2" style="height: 3rem;" alt="${decor.name}">
          <div>${decor.name}</div>
        </div>
    `);
    }
  };

  this.renderProducts = () => {
    $("#checkoutContainer").append(
      `<div id="product-final" class="col-md-6 row g-3 align-items-start">
        <h5 class="w-100">Products</h5>
      </div>`
    );
    const pFinal = toArray(products);
    for (let i = 0; i < pFinal.length; i++) {
      const product = pFinal[i];
      const productDetail = ProductScreen.products.find((x) => x.id === pFinal[i].id);
      $("#product-final").append(`<div class="col-6">
        <div class="d-flex align-items-start w-100 pb-3 mb-3">
            <img src="${productDetail.img}" class="rounded-circle me-2" style="height: 6rem;" alt="${productDetail.name}">
            <div class="d-flex flex-column flex-fill align-items-start">
                <div>${productDetail.name}</div>
                <div><span class="badge rounded-pill bg-secondary my-1">${productDetail.unit} unit</span> x ${product.qty}</div>
            </div>
        </div>
      </div>`);
    }
  };

  this.renderCheckout = () => {
    $("#checkoutContainer").append(
      `<div class="col-6 mx-auto">
            <div onclick="CheckoutScreen.checkout()" class="btn btn-primary w-100">Checkout</div>
        </div>`
    );
  };

  this.checkout = () => {
    var size;
    switch (packaging.size) {
      case "Smol": size = "small";
      case "Modest": size = "medium";
      case "Huge": size = "large";
    }
    const decor = Array.from(decorations).map((d) => ({
      "jenisDekorasi": d,
    }));
    const packagingData = {
      decorations: decor,
      maxUnit: packaging.intSize,
      jenisPackage: size,
    }
    this.processPackaging(packagingData)
  };

  this.processPackaging = (packagingData) => {
    $.ajax({
      url: "/api/packaging",
      type: "post",
      data: JSON.stringify(packagingData),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    }).done(function (data) {
      packaging = data;
      CheckoutScreen.processCheckout();
    });

    this.processCheckout = () => {
      const checkoutJson = {
        "username": username,
        "createdAt": new Date().toISOString()
            .replace(/T/, ' ')
            .replace(/\..+/, '')
            .replaceAll('-', '/')
            .slice(0, -3),
        "packaging": packaging,
        "products": toArray(products),
        "status": "Menunggu Konfirmasi",
      };
      console.log(checkoutJson);
      $.ajax({
        url: "/api/checkout/",
        type: "post",
        data: JSON.stringify(checkoutJson),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
      }).done(function (data) {
        swal({title: "Thank you!", text: "Your order is being processed.", icon: "success"}).then(() => {
          window.location.replace("/history/")
        });;
      });
    }
  }
})();
