const DecorationScreen = new (function () {
  this.start = () => {
    this.renderDecoration();
    HampersCart.renderCart(2);
    if (packaging.hasOwnProperty("size")) HampersCart.renderPackaging();
    if (toArray(products).length > 0) HampersCart.renderProducts();
    if (decorations.size > 0) HampersCart.renderDecoration();
  };

  this.decorationOption = [
    {
      img: "https://i.ebayimg.com/images/g/IeUAAOSwarlb1tGZ/s-l300.jpg",
      jenisDekorasi: "ucapan",
      name: "Greeting Cards",
    },
    {
      img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvOHCg9CO1HlzbtGfDYZUZScOjHA5wZ2I25Q&usqp=CAU",
      jenisDekorasi: "pita",
      name: "Ribbons",
    },
    {
      img: "https://images-na.ssl-images-amazon.com/images/I/71JHRALGy6L._AC_SL1000_.jpg",
      jenisDekorasi: "stiker",
      name: "Stickers",
    },
  ];

  this.renderDecoration = () => {
    $("#canvas").text("");
    $("#canvas").append(`<div class="container-fluid bg-light py-3 mb-4">  
        <div class="container position-relative">        
          <h1>Step 3</h1>
          <h3>Add decorations.</h3>
          <div class="btn-group" style="position: absolute; bottom: 0; right: 0;">
            <div onclick="ProductScreen.start()" class="btn btn-outline-secondary">Previous</div>
            <div onclick="DecorationScreen.next()" class="btn btn-primary">Next</div>
          </div>
        </div>        
      </div>
      <div class="container">
        <div id="decorContainer" class="row g-3"></div>
      </div>`);
    for (var i = 0; i < this.decorationOption.length; i++) {
      const decor = this.decorationOption[i];
      $("#decorContainer").append(`
          <div class="col-md-4 col-6">
              <div class="text-center">
                <img src="${decor.img}" class="rounded-circle w-75 mx-auto" alt="${decor.name}">
                <div class="card-body d-flex flex-column align-items-center">
                  <div class="fs-5 fw-bold mb-1">${decor.name}</div>
                  <div id="add-${i}" onclick="DecorationScreen.addToCart('${decor.jenisDekorasi}')" class="button btn btn-primary mx-auto mt-2"><i class="fa fa-shopping-cart"></i> Add to cart</div>
                </div>
              </div>                    
          </div>
          `);
    }
  };

  this.addToCart = (jenisDekorasi) => {
    decorations.add(jenisDekorasi);
    const decor = this.decorationOption.find((x) => x.jenisDekorasi === jenisDekorasi);
    new Toast({
      message: `${decor.name} added to cart.`,
      type: "success",
    });
    this.renderCart();
  };

  this.removeFromCart = (jenisDekorasi) => {
    decorations.delete(jenisDekorasi);
    const decor = this.decorationOption.find((x) => x.jenisDekorasi === jenisDekorasi);
    new Toast({
      message: `${decor.name} removed from cart.`,
      type: "success",
    });
    this.renderCart();
  };

  this.renderCart = () => {
    $("#decorations").empty();
    const a = Array.from(decorations);
    if (a.length === 0) {
      $("#decorations").append(`
        <div class="d-flex align-items-center">
          <div style="width: 1rem; height: 1rem; border-radius: 50%; background-color: rosybrown; margin-right: .5rem"></div> No decorations chosen
        </div>  
      `)
    } else {
      for (let i = 0; i < a.length; i++) {
        const decor = this.decorationOption.find((x) => x.jenisDekorasi === a[i]);
        $("#decorations").append(`
        <div onMouseOver="this.style.background='bisque'" onMouseOut="this.style.background='transparent'" onclick="DecorationScreen.removeFromCart('${decor.jenisDekorasi}')" class="badge rounded-pill d-flex align-items-center me-2 px-2 py-1 text-nowrap" style="border: 1px solid rosybrown; color: rosybrown; cursor: pointer;">
        <img src="${decor.img}" style="width: 1rem; height: 1rem; border-radius: 50%; margin-right: .2rem">${decor.name}<i class="far fa-times-circle text-danger ms-1"></i>
        </div>
        `);
      }
    }
  };

  this.next = () => {
    CheckoutScreen.start();
  };
})();
