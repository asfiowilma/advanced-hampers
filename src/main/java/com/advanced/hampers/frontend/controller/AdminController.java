package com.advanced.hampers.frontend.controller;

import com.advanced.hampers.backend.service.CheckoutService;
import com.advanced.hampers.backend.service.StockManagementService;
import com.advanced.hampers.backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/admin")
public class AdminController {

    @Autowired
    private StockManagementService stockManagementService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CheckoutService checkoutService;

    @GetMapping("")
    public String admin(Model model) {
        model.addAttribute("Products", productService.getAllProduct());
        return "admin/admin";
    }

    @GetMapping("confirm")
    public String confirm(Model model) {
        model.addAttribute("Transaksi", checkoutService.getAllSummary());
        return "admin/confirm";
    }
}
