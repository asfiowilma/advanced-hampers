package com.advanced.hampers.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/checkout")
public class CheckoutController {

    @RequestMapping(path = "", method = RequestMethod.GET)
    public String checkoutSummary() {
        //ambil package sama content
        //model.addAttribute();
        //model.addAttribute();
        return "checkout/summary";
    }


}
