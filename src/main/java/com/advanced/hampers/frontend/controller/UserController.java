package com.advanced.hampers.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.advanced.hampers.backend.model.User;

@Controller
public class UserController {
    
    @GetMapping("/registration")
    public String registration(Model model) throws Exception{
        User userDto = new User();
        model.addAttribute("user", userDto);
        return "auth/registration";
    }
}
