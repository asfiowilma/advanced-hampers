package com.advanced.hampers.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/history")
public class HistoryController {

    @GetMapping(path="")
    public String getAllHistory() {
        // get all successful order
        return "history/history";
    }

    @GetMapping(path="/{id}")
    public String getOrderById(@PathVariable(value = "id") int id) {
        // get order by id
        return "history/details";
    }
}
