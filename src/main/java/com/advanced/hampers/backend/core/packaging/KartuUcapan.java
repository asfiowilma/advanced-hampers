package com.advanced.hampers.backend.core.packaging;

import java.util.List;

public class KartuUcapan extends PackagingDecorator{

    Packaging packaging;

    public KartuUcapan(Packaging packaging){
        this.packaging = packaging;
    }

    @Override
    public String getDescription() {
        return packaging.getDescription() + ", kartu ucapan";
    }

    @Override
    public int getCapacity() {
        return packaging.getCapacity();
    }

    @Override
    public List<String> getDecorations() {
        List<String> decList = packaging.getDecorations();
        decList.add("kartu ucapan");
        return decList;
    }
}
