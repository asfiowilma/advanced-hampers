package com.advanced.hampers.backend.core.packaging;

import java.util.List;

public class Stiker extends PackagingDecorator {

    Packaging packaging;

    public Stiker(Packaging packaging) {
        this.packaging = packaging;
    }

    @Override
    public String getDescription() {
        return packaging.getDescription() + ", stiker";
    }

    @Override
    public int getCapacity() {
        return packaging.getCapacity();
    }

    @Override
    public List<String> getDecorations() {
        List<String> decList = packaging.getDecorations();
        decList.add("stiker");
        return decList;
    }
}
