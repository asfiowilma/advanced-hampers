package com.advanced.hampers.backend.core.packaging;

import java.util.ArrayList;
import java.util.List;

public class MediumPackaging extends Packaging{

    List<String> decorationList;

    public MediumPackaging(){
        description = "Medium packaging";
        capacity = 25;
        decorationList = new ArrayList<>();
    }

    @Override
    public List<String> getDecorations() {
        return decorationList;
    }
}
