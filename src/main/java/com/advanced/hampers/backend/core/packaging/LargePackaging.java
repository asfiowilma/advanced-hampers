package com.advanced.hampers.backend.core.packaging;

import java.util.ArrayList;
import java.util.List;

public class LargePackaging extends Packaging{

    List<String> decorationList;

    public LargePackaging(){
        description = "Large packaging";
        capacity = 50;
        decorationList = new ArrayList<>();
    }

    @Override
    public List<String> getDecorations() {
        return decorationList;
    }

}
