package com.advanced.hampers.backend.core.packaging;

import java.util.ArrayList;
import java.util.List;

public class SmallPackaging extends Packaging{

    List<String> decorationList;

    public SmallPackaging(){
        description = "Small packaging";
        capacity = 10;
        decorationList = new ArrayList<>();
    }

    @Override
    public List<String> getDecorations() {
        return decorationList;
    }
}
