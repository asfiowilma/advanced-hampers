package com.advanced.hampers.backend.core.packaging;

import java.util.List;

public class Pita extends PackagingDecorator{

    Packaging packaging;

    public Pita(Packaging packaging){
        this.packaging = packaging;
    }

    @Override
    public String getDescription() {
        return packaging.getDescription() + ", pita";
    }

    @Override
    public int getCapacity() {
        return packaging.getCapacity();
    }

    @Override
    public List<String> getDecorations() {
        List<String> decList = packaging.getDecorations();
        decList.add("pita");
        return decList;
    }
}
