package com.advanced.hampers.backend.core.packaging;

import java.util.List;

public abstract class Packaging {

    String description = "Unknown packaging";

    int capacity = 0;

    public String getDescription(){
        return description;
    }

    public int getCapacity(){
        return capacity;
    }

    public abstract List<String> getDecorations();

}
