package com.advanced.hampers.backend.core.packaging;

public abstract class PackagingDecorator extends Packaging{

    public abstract String getDescription();

    public abstract int getCapacity();

}
