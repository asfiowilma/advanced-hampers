package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.security.HampersUserDetails;

public interface SecurityService {
    HampersUserDetails findLoggedInUserDetails();

    void autoLogin(String username, String password);
}
