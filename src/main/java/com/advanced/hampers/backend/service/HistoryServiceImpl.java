package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Checkout;
import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.model.ProductSummary;
import com.advanced.hampers.backend.repository.HistoryRepository;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class HistoryServiceImpl implements HistoryService{

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Checkout> getAllOrder() {
        return historyRepository.findAll();
    }

    @Override
    public Checkout getOrderById(int id) {
        return historyRepository.findById(id);
    }

    @Override
    public List<Checkout> getOrderByUsername(String username) {
        List<Checkout> res = new ArrayList<>();
        List<Checkout> orders = getAllOrder();
        for (int i = 0; i < orders.size(); i++) {
            Checkout order = orders.get(i);
            if (order.getUser().getUsername().equals(username)) {
                res.add(order);
            }
        }
        return res;
    }

    @Override
    public Iterable<Product> getBestselling() {
        List<Checkout> orders = getAllOrder();
        HashMap<Integer, Integer> productsMap = new HashMap<>();

        for (int i = 0; i < orders.size(); i++) {
            Checkout order = orders.get(i);
            for(ProductSummary ps : order.getProductSummaries()) {
                int product = ps.getProduct().getId();
                if (!productsMap.containsKey(product)) {
                    productsMap.put(product, ps.getQty());
                } else {
                    int num = productsMap.get(product);
                    productsMap.put(product, num + ps.getQty());
                }
            }
        }
        int[] qty = new int[4];
        HashSet<Integer> added = new HashSet<>();
        for (int productId : productsMap.keySet()) {
            System.out.println(productId + "-" + productsMap.get(productId));
            if (!added.contains(productId)) {
                added.add(productId);
                for (int i = 0; i < qty.length; i++) {
                    if (qty[i] != 0) {
                        if (productsMap.get(productId) > productsMap.get(qty[i])) {
                            qty[i] = productId;
                            break;
                        }
                    } else {
                        qty[i] = productId;
                        break;
                    }
                }
            }
        }
        List<Product> bestsellers = new ArrayList<>();
        for (int i = 0; i <qty.length; i++) {
            Product product = productRepository.findById(qty[i]);
            bestsellers.add(product);
        }
        return bestsellers;
    }


}
