package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Product;

import java.util.HashMap;
import java.util.List;

public interface StockManagementService {
    Product updateStock(int id, int stock);
    List<Product> bulkUpdateStock(HashMap<Integer,Integer> newStock);
    List<Product> getAvailableProducts();
    List<Product> getOutOfStockProducts();
    List<Product> getLowStockProducts();
}
