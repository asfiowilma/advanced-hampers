package com.advanced.hampers.backend.service;


import com.advanced.hampers.backend.model.PackagingModel;
import com.advanced.hampers.backend.core.packaging.Packaging;

import java.util.Map;

public interface PackagingService {

    Packaging selectPackaging(Map<String, String> map);

    PackagingModel createPackaging(PackagingModel packaging);
    Iterable<PackagingModel> getListPackaging();
    PackagingModel getPackagingById(int id);
    PackagingModel updatePackaging(int id, PackagingModel packaging);
    void deletePackagingById(int id);
}
