package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Checkout;
import com.advanced.hampers.backend.model.Product;

import java.util.List;

public interface HistoryService {
    List<Checkout> getAllOrder();
    Checkout getOrderById(int id);
    List<Checkout> getOrderByUsername(String username);
    Iterable<Product> getBestselling();
}
