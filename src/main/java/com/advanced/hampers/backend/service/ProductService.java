package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Product;

import java.util.List;

public interface ProductService {
    Product createProduct(Product product);
    Product getProductById(int id);
    Product getProductByName(String name);
    List<Product> getAllProduct();

    Product updateProduct(int id, Product product);
    void deleteProductById(int Id);
}
