package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class StockManagementServiceImpl implements StockManagementService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product updateStock(int id, int stock) {
        Product product = productRepository.findById(id);
        if (stock < 0 || product == null) {
            return product;
        }
        product.setStock(stock);
        return productRepository.save(product);
    }

    @Override
    public List<Product> bulkUpdateStock(HashMap<Integer, Integer> newStock) {
        List<Product> productList = new ArrayList<Product>();
        int stock;
        for (Product product : productRepository.findAllById(newStock.keySet())) {
            stock = newStock.get(product.getId());
            if (stock < 0) {
                productList.add(product);
                continue;
            }
            product.setStock(stock);
            productList.add(product);
        }
        return productRepository.saveAll(productList);
    }

    @Override
    public List<Product> getAvailableProducts() {
        List<Product> productList = new ArrayList<Product>();
        for (Product product : productRepository.findAll()) {
            if (product.getStock() > 0) {
                productList.add(product);
            }
        }
        return productList;
    }

    @Override
    public List<Product> getOutOfStockProducts() {
        List<Product> productList = new ArrayList<Product>();
        for (Product product : productRepository.findAll()) {
            if (product.getStock() == 0) {
                productList.add(product);
            }
        }
        return productList;
    }

    @Override
    public List<Product> getLowStockProducts() {
        List<Product> productList = new ArrayList<Product>();
        for (Product product : productRepository.findAll()) {
            if (product.getStock() < 10) {
                productList.add(product);
            }
        }
        return productList;
    }
}
