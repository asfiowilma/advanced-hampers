package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Checkout;
import com.advanced.hampers.backend.model.Decoration;
import com.advanced.hampers.backend.model.PackagingModel;
import com.advanced.hampers.backend.model.ProductSummary;
import com.advanced.hampers.backend.repository.CheckoutRepository;
import com.advanced.hampers.backend.repository.PackagingRepository;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.hibernate.annotations.Check;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import java.awt.*;
import java.util.List;


@Service
public class CheckoutServiceImpl implements CheckoutService {

    @Autowired
    private CheckoutRepository checkoutRepository;
    @Autowired
    private PackagingService packagingService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;

    @Override
    public Checkout createSummary(Checkout checkout){
        List<ProductSummary> productSummaryList = generateProductSummaries(checkout.getProducts());
        for (ProductSummary ps: productSummaryList){
            ps.setOrderId(checkout.getOrderId());
        }
        checkout.setProductSummaries(productSummaryList);
        checkout.setUser(userService.findByUsername(checkout.getUsername()));
        checkoutRepository.save(checkout);
        return checkout;
    }

    @Override
    public List getDecorations(int id) {
        return checkoutRepository.findOrderByOrderId(id).getPackaging().getDecorations();
    }

    @Override
    public Set getProductIdByOrderId(int id) {
        Set<Integer> productId = new HashSet<>();
        for(ProductSummary ps : checkoutRepository.findOrderByOrderId(id).getProductSummaries()){
            productId.add(ps.getProduct().getId());
        }

        return productId;
    }

    @Override
    public List generateProductSummaries(List<Map<String, Object>> products) {
        List<ProductSummary> result = new ArrayList<>();
        for (Map<String, Object> product : products) {
            if (product != null) {
                ProductSummary ps = new ProductSummary(
                        productService.getProductById((int) product.get("id")), (int) product.get("qty")
                );
                result.add(ps);
            }
        }
        return result;
    }

    @Override
    public List<Checkout> getAllSummary() {
        return checkoutRepository.findAll();
    }

    @Override
    public Checkout getSummaryById(int id) {
        return checkoutRepository.findById(id);
    }

    @Override
    public void deleteSummaryById(int id) {
        checkoutRepository.deleteById(id);
    }

    @Override
    public Checkout ubahStatus(int id, String status) {
        Checkout checkout = checkoutRepository.findById(id);
        checkout.setStatus(status);
        return checkoutRepository.save(checkout);
    }
}
