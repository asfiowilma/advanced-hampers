package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.User;
import org.springframework.validation.Errors;

public interface UserService {

    User findByUsername(String username);

    User register(User user);
}
