package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Checkout;

import java.util.*;

public interface CheckoutService {
    Checkout createSummary(Checkout checkout);
    List getDecorations(int id);
    Set getProductIdByOrderId(int id);
    List generateProductSummaries(List<Map<String, Object>> products);
    //CheckoutProducts getQuantityByProductId(int id);
    List<Checkout> getAllSummary();
    Checkout getSummaryById(int id);
    void deleteSummaryById(int Id);
    Checkout ubahStatus(int id, String status);
}
