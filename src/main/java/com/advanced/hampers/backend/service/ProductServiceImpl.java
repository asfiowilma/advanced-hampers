package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.advanced.hampers.backend.service.ProductService;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product getProductById(int idProduct) {
        return productRepository.findById(idProduct);
    }

    @Override
    public Product getProductByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product updateProduct(int id, Product product) {
        Product oldProduct = productRepository.findById(id);
        oldProduct.setName(product.getName());
        oldProduct.setUnit(product.getUnit());
        oldProduct.setImg(product.getImg());
        return productRepository.save(oldProduct);
    }

    @Override
    public void deleteProductById(int id) {
        productRepository.deleteById(id);
    }
}
