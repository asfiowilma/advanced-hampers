package com.advanced.hampers.backend.service;



import com.advanced.hampers.backend.model.Decoration;
import com.advanced.hampers.backend.model.PackagingModel;
import com.advanced.hampers.backend.repository.DecorationRepository;
import com.advanced.hampers.backend.repository.PackagingRepository;



//import com.advanced.hampers.backend.repository.PackagingRepository;
import com.advanced.hampers.backend.core.packaging.KartuUcapan;
import com.advanced.hampers.backend.core.packaging.LargePackaging;
import com.advanced.hampers.backend.core.packaging.MediumPackaging;
import com.advanced.hampers.backend.core.packaging.Packaging;
import com.advanced.hampers.backend.core.packaging.Pita;
import com.advanced.hampers.backend.core.packaging.SmallPackaging;
import com.advanced.hampers.backend.core.packaging.Stiker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PackagingServiceImpl implements PackagingService{

    @Autowired
    private PackagingRepository packagingRepository;

    @Autowired
    private DecorationRepository decorationRepository;

    @Override
    public Packaging selectPackaging(Map<String, String> map) {
        String size = map.get("size");

        Packaging packaging;

        if(size.equals("small")){
            packaging = new SmallPackaging();
        }else if(size.equals("medium")){
            packaging = new MediumPackaging();
        }else{
            packaging = new LargePackaging();
        }


        if(map.get("decoration1") != null){
            packaging = new Pita(packaging);
        }
        if(map.get("decoration2") != null){
            packaging = new Stiker(packaging);
        }
        if(map.get("decoration3") != null){
            packaging = new KartuUcapan(packaging);
        }

        return packaging;
    }

    @Override
    public PackagingModel createPackaging(PackagingModel packaging) {
        for (Decoration dec: packaging.getDecorations()){
            dec.setPackaging(packaging);
        }
        packagingRepository.save(packaging);
        return packaging;
    }

    @Override
    public Iterable<PackagingModel> getListPackaging() {
        return packagingRepository.findAll();
    }

    @Override
    public PackagingModel getPackagingById(int id) {
        return packagingRepository.findByPackagingId(id);
    }

    @Override
    public PackagingModel updatePackaging(int id, PackagingModel packaging) {
        PackagingModel oldPackaging = packagingRepository.findByPackagingId(id);
        oldPackaging.setJenisPackage(packaging.getJenisPackage());
        oldPackaging.setMaxUnit(packaging.getMaxUnit());

        List<Integer> idsToDel = new ArrayList<>();

        for (Decoration oldDec : oldPackaging.getDecorations()){
            oldDec.setPackaging(null);
            idsToDel.add(oldDec.getId());
        }
        oldPackaging.setDecorations(packaging.getDecorations());
        for (Decoration dec : packaging.getDecorations()){
            dec.setPackaging(oldPackaging);
        }
        packagingRepository.save(oldPackaging);


        try{
            if (idsToDel.size() > 0){
                for (int i = 0; i < idsToDel.size(); i++){
                    decorationRepository.deleteById(idsToDel.get(i));
                }
            }
        }catch (Exception e){
            System.out.println(e);
            e.printStackTrace();
        }

        return oldPackaging;
    }

    @Override
    public void deletePackagingById(int id) {
        packagingRepository.deleteById(id);
    }
}
