package com.advanced.hampers.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "product_summary")
@Data
@NoArgsConstructor
public class ProductSummary {

    @Id
    @Column(name = "orderId", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private int orderId;


//    @ManyToOne
//    @JoinColumn(name = "checkoutId")
//    @JsonIgnore
//    private Checkout checkout;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;

    @Column(name = "Qty")
    private int qty;

    public ProductSummary(Product product, int qty){
//        this.checkout = checkout;
        this.product = product;
        this.qty = qty;
    }



}
