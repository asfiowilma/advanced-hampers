package com.advanced.hampers.backend.model;

import com.advanced.hampers.backend.model.Decoration;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "packaging")
@Data
@NoArgsConstructor
public class PackagingModel {


    @Id
    @Column(name = "packagingId", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int packagingId;

    @Column(name = "jenis_package")
    private String jenisPackage;

    @Column(name = "max_unit")
    private int maxUnit;


//    @JsonIgnore
//    orphanRemoval = true

    @OneToMany(
            mappedBy = "packaging",
            cascade = CascadeType.ALL
    )
    private List<Decoration> decorations;

    public PackagingModel(String jenisPackage, int maxUnit, List<Decoration> decorations) {
        this.jenisPackage = jenisPackage;
        this.maxUnit = maxUnit;
        this.decorations = decorations;
    }
}
