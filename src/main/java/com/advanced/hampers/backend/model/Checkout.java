package com.advanced.hampers.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Entity
@Table(name = "checkout")
@Data
@NoArgsConstructor

public class Checkout {

    @Id
    @Column(name = "orderId", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderId;

    @ManyToOne
    @JoinColumn(name = "id")
    @JsonIgnore
//    @Column(name = "username")
    private User user;

    @ManyToOne
    @JoinColumn(name = "packaging_id")
    private PackagingModel packaging;

    @Column(name = "createdAt")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime createdAt;


    @OneToMany(targetEntity = ProductSummary.class,cascade = CascadeType.ALL)
//    @JoinColumn(name = "id",referencedColumnName = "id")
    private List<ProductSummary> productSummaries;

    @Column(name = "status")
    private String status;

    @Transient
    private List<Map<String, Object>> products;
    @Transient
    private String username;

    @JsonGetter("pembeli")
    public String getPembeli() {
        return user.getUsername();
    }

    public Checkout(User user,PackagingModel packaging,String createdAt,List<ProductSummary> productSummaries,String status) {
        this.user = user;
        this.packaging = packaging;
        this.createdAt = LocalDateTime.parse(createdAt, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"));
        this.productSummaries = productSummaries;
        this.status = status;
    }

}
