package com.advanced.hampers.backend.model;

import com.advanced.hampers.backend.security.HampersUserDetails;
import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "hampers_user")
@Data
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String username;
    @Column
    private String password;
    @Transient
    private String passwordConfirm;
    @Column
    private boolean active;
    @Column
    private String roles;

    public User(String userName, String password, String roles) {
        this.username = userName;
        this.password = password;
        this.active = true;
        this.roles = roles;
    }
}
