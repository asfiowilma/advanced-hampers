package com.advanced.hampers.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "product")
@Data
@NoArgsConstructor
public class Product {

    @Id
    @Column(name = "id", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "unit")
    private int unit;

    @Column(name = "name")
    private String name;

    @Column(name = "img")
    private String img;

    @Column(name = "stock")
    private int stock;

    public Product(int unit, String name, String img) {
        this.unit = unit;
        this.name = name;
        this.img = img;
        this.stock = 0;
    }
}