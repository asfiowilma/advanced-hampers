package com.advanced.hampers.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "decoration")
@Data
@NoArgsConstructor
public class Decoration {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "jenis_dekorasi")
    private String jenisDekorasi;

    @ManyToOne
    @JoinColumn(name = "packaging_id")
    @JsonIgnore
    private PackagingModel packaging;

    public Decoration(String jenisDekorasi, PackagingModel packaging) {
//        this.id = id;
        this.jenisDekorasi = jenisDekorasi;
        this.packaging = packaging;
    }
}
