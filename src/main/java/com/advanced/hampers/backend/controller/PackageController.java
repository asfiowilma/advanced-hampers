package com.advanced.hampers.backend.controller;


import com.advanced.hampers.backend.service.PackagingService;
import com.advanced.hampers.backend.core.packaging.Packaging;
import com.advanced.hampers.backend.service.PackagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping(path = "/package")
public class PackageController {

    @Autowired
    private PackagingService packagingService;

//    @GetMapping("/")
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String choosePackage(Model model){
        return "package/packageSelection";
    }

//    @RequestMapping(path = "/selected", method = RequestMethod.GET)
//    public String selected(@RequestParam Map<String, String> params, Model model){
//
////        System.out.println(params.keySet());
////        System.out.println(params.values());
//
//        Map<String,String> selection = new HashMap<>();
//        selection.put("size", null);
//        selection.put("decoration1", null);
//        selection.put("decoration2", null);
//        selection.put("decoration3", null);
//
//        for (String keys: params.keySet()){
//            selection.put(keys,params.get(keys));
//        }
//
////        model.addAttribute("size", selection.get("size"));
////        model.addAttribute("decoration1", selection.get("decoration1"));
////        model.addAttribute("decoration2", selection.get("decoration2"));
////        model.addAttribute("decoration3", selection.get("decoration3"));
//
//        Packaging packaging = packagingService.selectPackaging(selection);
//        model.addAttribute("packaging", packaging);
//
//        return "package/selectedPackage";
//    }


}
