package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.*;
import com.advanced.hampers.backend.service.CheckoutService;
import com.advanced.hampers.backend.service.PackagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/checkout")
public class CheckoutApiController {
    @Autowired
    CheckoutService checkoutService;

    private Checkout checkout1;
    private PackagingModel pg;
    private ProductSummary ps;
    private Product p;
    private User user;

    @PostMapping(path="/",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Checkout> createSummary(@RequestBody Checkout checkout){
        return ResponseEntity.ok(checkoutService.createSummary(checkout));
    }

    @GetMapping(path="/",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getSummary() {
//        p =  new Product(3,"Brownies","https://images.unsplash.com/photo-1592177538809-f713fd7d82c9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80");
//        user = new User("logintest","logintest","ADMIN, USER");
//        List<ProductSummary> productId = new ArrayList<>();
//        List<Decoration> dc = new ArrayList<>();
//        dc.add(new Decoration("Pita",pg));
//        ps = new ProductSummary(p,2);
//        productId.add(ps);
//        pg = new PackagingModel("Besar",60,dc);
//        checkout1 = new Checkout(user,pg,"2020/03/12 12:12", productId,"Menunggu Konfirmasi");
        return ResponseEntity.ok(checkoutService.getAllSummary());
    }

    @GetMapping(path="/{id}/summary", produces = {"application/json"})
    public ResponseEntity getSummaryById(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(checkoutService.getSummaryById(id));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteSummary(@PathVariable(value = "id") int id) {
        checkoutService.deleteSummaryById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/", produces = {"application/json"})
    public ResponseEntity ubahStatus(@RequestBody Map<String, Object> payload) {
        return ResponseEntity.ok(checkoutService.ubahStatus(Integer.parseInt((String) payload.get("id")), (String) payload.get("status")));
    }

    @GetMapping(path="/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Set<Integer>> getProductIdByOrderId(@PathVariable(value = "id") int id) {
        Set<Integer> productId = checkoutService.getProductIdByOrderId(id);
        return productId == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(productId);
    }

    @GetMapping(path="/d/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Decoration>> getDecorationsById(@PathVariable(value = "id") int id) {
        List dc = checkoutService.getDecorations(id);
        return dc == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(dc);
    }
}
