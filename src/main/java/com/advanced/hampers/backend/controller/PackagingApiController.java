package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.PackagingModel;
import com.advanced.hampers.backend.service.PackagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/packaging")
public class PackagingApiController {

    @Autowired
    PackagingService packagingService;


    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postPackaging(@RequestBody PackagingModel packaging){
//        System.out.println("JENIS:" + packaging.getJenisPackage());
        return ResponseEntity.ok(packagingService.createPackaging(packaging));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PackagingModel>> getListPackaging(){
        return ResponseEntity.ok(packagingService.getListPackaging());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPackaging(@PathVariable(value = "id") int id){
        PackagingModel packaging = packagingService.getPackagingById(id);
        if(packaging == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(packaging);
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updatePackaging(@PathVariable(value = "id") int id, @RequestBody PackagingModel packaging) {
        return ResponseEntity.ok(packagingService.updatePackaging(id, packaging));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deletePackaging(@PathVariable(value = "id") int id){
        packagingService.deletePackagingById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
