package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.service.StockManagementService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/stock-management")
public class StockManagementApiController {

    @Autowired
    private StockManagementService stockManagementService;

    @GetMapping(path="/available", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getAvailableProducts() {
        return ResponseEntity.ok(stockManagementService.getAvailableProducts());
    }

    @GetMapping(path="/outofstock", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getOutOfStockProducts() {
        return ResponseEntity.ok(stockManagementService.getOutOfStockProducts());
    }

    @GetMapping(path="/low", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getLowStockProducts() {
        return ResponseEntity.ok(stockManagementService.getLowStockProducts());
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Product> updateStock(@PathVariable(value = "id") int id, @RequestBody Map<String, Object> payload) {
        return ResponseEntity.ok(stockManagementService.updateStock(id, (int) payload.get("jumlah")));
    }

    @PutMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Product>> bulkUpdateStock(@RequestBody JsonNode jsonNode) {
        HashMap<Integer, Integer> newStock = new HashMap<Integer, Integer>();
        for (int i = 0; i < jsonNode.size(); i++) {
            newStock.put(jsonNode.get(i).get("id").asInt(), jsonNode.get(i).get("jumlah").asInt());
        }
        return ResponseEntity.ok(stockManagementService.bulkUpdateStock(newStock));
    }
}
