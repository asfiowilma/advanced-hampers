package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Checkout;
import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.service.HistoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/history")
public class HistoryApiController {

    @Autowired
    private HistoryServiceImpl historyService;

    @GetMapping(path="/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Checkout>> getAllHistory() {
        return ResponseEntity.ok(historyService.getAllOrder());
    }

    @GetMapping(path="/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Checkout> getHistoryById(@PathVariable(value = "id") int id) {
        Checkout order = historyService.getOrderById(id);
        return order == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(order);
    }

    @GetMapping(path="/u/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Checkout>> getHistoryByUsername(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(historyService.getOrderByUsername(username));
    }

    @GetMapping(path="/bestsellers", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getBestsellers() {
        return ResponseEntity.ok(historyService.getBestselling());
    }
}
