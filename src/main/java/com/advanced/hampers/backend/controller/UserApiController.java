package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.model.User;
import com.advanced.hampers.backend.security.HampersUserDetails;
import com.advanced.hampers.backend.security.UserValidator;
import com.advanced.hampers.backend.service.SecurityService;
import com.advanced.hampers.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserApiController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/api/user")
    public ResponseEntity<HampersUserDetails> userDetails() {
        return ResponseEntity.ok(securityService.findLoggedInUserDetails());
    }

    @PostMapping("/api/register")
    public String register(@ModelAttribute("user") User user, BindingResult bindingResult) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "auth/registration";
        }
        userService.register(user);
        securityService.autoLogin(user.getUsername(), user.getPasswordConfirm());

        return "redirect:/";
    }

}
