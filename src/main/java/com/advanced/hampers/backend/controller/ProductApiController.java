package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/product")
public class ProductApiController {

    @Autowired
    private ProductService productService;

    @GetMapping(path="/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProduct());
    }

    @GetMapping(path="/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Product> getProductById(@PathVariable(value = "id") int id) {
        Product product = productService.getProductById(id);
        return product == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(product);
    }

    @GetMapping(path="/n/{name}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Product> getProductById(@PathVariable(value = "name") String name) {
        Product product = productService.getProductByName(name);
        return product == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(product);
    }


    @PostMapping(path="/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        return ResponseEntity.ok(productService.createProduct(product));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Product> updateMahasiswa(@PathVariable(value = "id") int id, @RequestBody Product product) {
        return ResponseEntity.ok(productService.updateProduct(id, product));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteMahasiswa(@PathVariable(value = "id") int id) {
        productService.deleteProductById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}