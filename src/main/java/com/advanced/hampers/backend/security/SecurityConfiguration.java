package com.advanced.hampers.backend.security;

import com.advanced.hampers.backend.service.HampersUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    HampersUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.httpBasic().authenticationEntryPoint(new NoPopupBasicAuthenticationEntryPoint())
            .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/", true)
                .permitAll()
            .and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/registration").permitAll()
                .antMatchers("/api/register").permitAll()
                .antMatchers("/scripts/**", "/images/**", "/style.css").permitAll()
                .antMatchers("/checkout/").permitAll()
                .antMatchers("/api/history/bestsellers").permitAll()
                .antMatchers(HttpMethod.GET,"/api/stock-management/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers(HttpMethod.POST,"/api/stock-management/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/stock-management/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/stock-management/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/product/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/product/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/product/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET,"/api/packaging/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers(HttpMethod.POST,"/api/packaging/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT,"/api/packaging/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/packaging/**").hasAuthority("ADMIN")
                .antMatchers("/**").authenticated()
            .and()
                .logout()
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/login?logout=true")
                .invalidateHttpSession(true)
                .permitAll()
            .and()
                .cors().and().csrf().disable();
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

