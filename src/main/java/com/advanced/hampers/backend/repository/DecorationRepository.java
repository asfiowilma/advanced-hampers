package com.advanced.hampers.backend.repository;

import com.advanced.hampers.backend.model.Decoration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DecorationRepository extends JpaRepository<Decoration, Integer> {
    Decoration findById(int id);

}
