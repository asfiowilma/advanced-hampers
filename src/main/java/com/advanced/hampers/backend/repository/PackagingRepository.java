package com.advanced.hampers.backend.repository;

import com.advanced.hampers.backend.model.PackagingModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PackagingRepository extends JpaRepository<PackagingModel, Integer> {
    PackagingModel findByPackagingId(int id);
}
