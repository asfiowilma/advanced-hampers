package com.advanced.hampers.backend.repository;

import com.advanced.hampers.backend.model.Checkout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRepository extends JpaRepository<Checkout, Integer> {
    Checkout findById(int id);
}
