package com.advanced.hampers.backend.repository;

import com.advanced.hampers.backend.model.ProductSummary;
import com.advanced.hampers.backend.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductSummaryRepository extends JpaRepository<ProductSummary, Integer> {
    ProductSummary findById(int id);
}
