package com.advanced.hampers.backend.repository;

import com.advanced.hampers.backend.model.Checkout;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CheckoutRepository extends JpaRepository<Checkout,Integer> {
    List<Checkout> findAll();
    //String findByCreatedAt(String createdAt);
    Checkout findById(int id);
    Checkout findOrderByOrderId(int id);


}
