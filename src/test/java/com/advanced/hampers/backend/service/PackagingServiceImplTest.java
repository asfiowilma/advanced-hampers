package com.advanced.hampers.backend.service;


import com.advanced.hampers.backend.core.packaging.Packaging;
import com.advanced.hampers.backend.model.Decoration;
import com.advanced.hampers.backend.model.PackagingModel;
import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.repository.PackagingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PackagingServiceImplTest {

    @Mock
    private PackagingRepository packagingRepository;

    @InjectMocks
    private PackagingServiceImpl packagingService;

    private PackagingModel packaging;

    @BeforeEach
    public void setUp() {
        List<Decoration> listdecorations = new ArrayList<>();
        Decoration dekorasi = new Decoration();
        listdecorations.add(dekorasi);
        packaging = new PackagingModel("small", 15, listdecorations);
        dekorasi.setPackaging(packaging);
    }


    @Test
    void createPackaging() {
        lenient().when(packagingService.createPackaging(packaging)).thenReturn(packaging);
    }

    @Test
    void getPackagingById() {
        lenient().when(packagingService.getPackagingById(packaging.getPackagingId())).thenReturn(packaging);
    }

    @Test
    void getListPackaging() {
        List<PackagingModel> listPackaging = packagingRepository.findAll();
        lenient().when(packagingService.getListPackaging()).thenReturn(listPackaging);
        Iterable<PackagingModel> listPackagingResult = packagingService.getListPackaging();
        Assertions.assertIterableEquals(listPackaging, listPackagingResult);
    }

    @Test
    void updatePackaging() {
        packagingService.createPackaging(packaging);
        String curJenis = packaging.getJenisPackage();
        packaging.setJenisPackage("HUGE");

        when(packagingRepository.findByPackagingId(packaging.getPackagingId())).thenReturn(packaging);
        lenient().when(packagingService.updatePackaging(packaging.getPackagingId(), packaging)).thenReturn(packaging);
        PackagingModel newPackaging = packagingService.updatePackaging(packaging.getPackagingId(), packaging);

        assertNotEquals(newPackaging.getJenisPackage(), curJenis);
        assertEquals(newPackaging.getJenisPackage(), packaging.getJenisPackage());
    }

    @Test
    void deletePackagingById() {
        packagingService.createPackaging(packaging);
        packagingService.deletePackagingById(packaging.getPackagingId());
        lenient().when(packagingService.getPackagingById(packaging.getPackagingId())).thenReturn(null);
        assertNull(packagingService.getPackagingById(packaging.getPackagingId()));
    }

    @Test
    void selectPackaging() {
        Map<String, String> map = new HashMap<>();

        map.put("size", "small");
        map.put("decoration1", "pita");
        map.put("decoration2", "stiker");
        map.put("decoration3", "kartu ucapan");

        Packaging selectedPackaging = packagingService.selectPackaging(map);
        assertEquals(10, selectedPackaging.getCapacity());
        assertEquals("Small packaging, pita, stiker, kartu ucapan", selectedPackaging.getDescription());

        map.remove("size");
        map.put("size", "large");

        Packaging selectedPackaging2 = packagingService.selectPackaging(map);
        assertEquals("Large packaging, pita, stiker, kartu ucapan", selectedPackaging2.getDescription());
    }
}
