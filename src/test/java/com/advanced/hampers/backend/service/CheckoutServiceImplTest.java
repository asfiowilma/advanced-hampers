package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.controller.CheckoutApiController;
import com.advanced.hampers.backend.controller.ProductApiController;
import com.advanced.hampers.backend.model.*;
import com.advanced.hampers.backend.service.CheckoutServiceImpl;
import com.advanced.hampers.backend.repository.CheckoutRepository;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;


@WebMvcTest(controllers = CheckoutApiController.class)
public class CheckoutServiceImplTest {

    @MockBean
    private CheckoutRepository checkoutRepository;

    @MockBean
    private PackagingService packagingService;
    @Mock
    private Decoration decoration;

    @MockBean
    private CheckoutServiceImpl checkoutService;

    @MockBean
    private HampersUserDetailsService userDetailsService;



    private Checkout checkout1;
    private PackagingModel pg;
    private ProductSummary ps;
    private Product p;
    private User user;


    @BeforeEach
    public void setUp() {
        p =  new Product(3,"Brownies","https://images.unsplash.com/photo-1592177538809-f713fd7d82c9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80");
        user = new User("logintest","logintest","ADMIN, USER");
        List<ProductSummary> productId = new ArrayList<>();
        List<Decoration> dc = new ArrayList<>();
        dc.add(new Decoration("Pita",pg));
        ps = new ProductSummary(p,2);
        productId.add(ps);
        pg = new PackagingModel("Besar",60,dc);
        checkout1 = new Checkout(user,pg,"2020/03/12 12:12", productId,"Menunggu Konfirmasi");
        //checkout2 = new Checkout(45,"24/03/2020",Collections.singleton(2));
    }

    @Test
    void createSummary(){
        lenient().when(checkoutRepository.save(checkout1)).thenReturn(checkout1);
    }

    @Test
    void getDecorations(){
        when(packagingService.createPackaging(pg)).thenReturn(pg);
        when(checkoutService.createSummary(checkout1)).thenReturn(checkout1);
        lenient().when(checkoutService.getDecorations(anyInt())).thenReturn(pg.getDecorations());
    }

    @Test
    void getDataByOrderId() {
        lenient().when(checkoutRepository.findOrderByOrderId(anyInt())).thenReturn(checkout1);

    }

    @Test
    void testGetAllSummary() {
        Iterable<Checkout> listSummary = Arrays.asList(checkout1);
        lenient().when(checkoutRepository.findAll()).thenReturn((List<Checkout>) listSummary);
//        Iterable<Checkout> listSummaryResult = checkoutService.getAllSummary();
//        Assertions.assertIterableEquals(listSummary, listSummaryResult);
    }

    @Test
    void testGetSummaryById() {
        lenient().when(checkoutRepository.findById(anyInt())).thenReturn(checkout1);
//        Checkout summaryResult = checkoutService.getSummaryById(anyInt());
//        Assertions.assertEquals(checkout1, summaryResult);
    }

    @Test
    void testDeleteSummaryById() {
        lenient().when(checkoutService.getSummaryById(checkout1.getOrderId())).thenReturn(null);
        checkoutService.deleteSummaryById(checkout1.getOrderId());
        assertNull(checkoutService.getSummaryById(checkout1.getOrderId()));
    }
}
