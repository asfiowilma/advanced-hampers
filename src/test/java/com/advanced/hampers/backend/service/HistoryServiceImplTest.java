package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.*;
import com.advanced.hampers.backend.repository.HistoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class HistoryServiceImplTest {

    @Mock
    private HistoryRepository historyRepository;

    @InjectMocks
    private HistoryServiceImpl historyService;

    private Checkout checkout;
    private PackagingModel pg;
    private ProductSummary ps;
    private Product p;
    private User user;

    @BeforeEach
    public void setUp() {
        p =  new Product(3,"Brownies","https://images.unsplash.com/photo-1592177538809-f713fd7d82c9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80");
        user = new User("logintest","logintest","ADMIN, USER");
        List<ProductSummary> productId = new ArrayList<>();
        List<Decoration> dc = new ArrayList<>();
        dc.add(new Decoration("Pita",pg));
        ps = new ProductSummary(p,2);
        productId.add(ps);
        pg = new PackagingModel("Besar",60,dc);
        checkout = new Checkout(user,pg,"2020/03/12 12:12", productId,"Menunggu Konfirmasi");
    }

    @Test
    void TestGetOrderById() {
        lenient().when(historyService.getOrderById(checkout.getOrderId())).thenReturn(checkout);
    }

    @Test
    void TestGetAllOrder() {
        List<Checkout> listOrders = historyRepository.findAll();
        lenient().when(historyService.getAllOrder()).thenReturn(listOrders);
        Iterable<Checkout> listOrdersRes = historyService.getAllOrder();
        Assertions.assertIterableEquals(listOrders, listOrdersRes);
    }

    @Test
    void TestGetOrderByUsername() {
        Iterable<Checkout> listOrder = Arrays.asList(checkout);
        lenient().when(historyService.getOrderByUsername(checkout.getUser().getUsername())).thenReturn((List<Checkout>) listOrder);
        Iterable<Checkout> listOrderRes = historyService.getOrderByUsername(checkout.getUser().getUsername());
        Assertions.assertIterableEquals(listOrder, listOrderRes );
    }
}
