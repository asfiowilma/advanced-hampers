package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    private Product product;

    @BeforeEach
    public void setUp() {
        product = new Product(1, "batu", "https://images.jpg");
    }

    @Test
    void createProduct() {
        lenient().when(productService.createProduct(product)).thenReturn(product);
    }

    @Test
    void getProductById() {
        lenient().when(productService.getProductById(product.getId())).thenReturn(product);
    }

    @Test
    void getProductByName() {
        lenient().when(productService.getProductByName("batu")).thenReturn(product);
    }

    @Test
    void getAllProduct() {
        List<Product> listProduct = productRepository.findAll();
        lenient().when(productService.getAllProduct()).thenReturn(listProduct);
        Iterable<Product> listProductResult = productService.getAllProduct();
        Assertions.assertIterableEquals(listProduct, listProductResult);
    }

    @Test
    void updateProduct() {
        productService.createProduct(product);
        String curName = product.getName();
        product.setName("bukan batu");

        when(productRepository.findById(product.getId())).thenReturn(product);
        lenient().when(productService.updateProduct(product.getId(), product)).thenReturn(product);
        Product newProduct = productService.updateProduct(product.getId(), product);

        assertNotEquals(newProduct.getName(), curName);
        assertEquals(newProduct.getName(), product.getName());
    }

    @Test
    void deleteProductById() {
        productService.createProduct(product);
        productService.deleteProductById(product.getId());
        lenient().when(productService.getProductById(product.getId())).thenReturn(null);
        assertNull(productService.getProductById(product.getId()));
    }
}