package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.User;
import com.advanced.hampers.backend.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User("user", "password", "USER");
    }

    @Test
    void register() {
        when(userRepository.save(user)).thenReturn(user);
        assertEquals(userService.register(user), user);
    }

    @Test
    void findByUsername() {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));
        assertEquals(userService.findByUsername(user.getUsername()), user);
    }

    @Test
    void findByUsernameNotFound() throws Exception {
        when(userRepository.findByUsername(user.getUsername()+"a")).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> userService.findByUsername(user.getUsername()+"a"));
    }

}