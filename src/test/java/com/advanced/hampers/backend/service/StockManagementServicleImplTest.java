package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StockManagementServicleImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private StockManagementServiceImpl stockManagementService;

    private Product product1;
    private Product product2;
    private Product product3;

    @BeforeEach
    public void setUp(){
        product1 = new Product(1, "batu", "https://images.jpg");
        product1.setStock(20);
        product2 = new Product(5, "bengbeng", "https://bengbeng.jpg");
        product2.setStock(0);
        product3 = new Product(15, "permen", "https://permen.jpg");
        product3.setStock(5);
    }

    @Test
    void testGetAvailableProducts() {
        Iterable<Product> listProduct = Arrays.asList(product1, product3);
        lenient().when(stockManagementService.getAvailableProducts()).thenReturn((List<Product>) listProduct);
        Iterable<Product> listProductResult = stockManagementService.getAvailableProducts();
        Assertions.assertIterableEquals(listProduct, listProductResult);
    }

    @Test
    void testGetOutOfStockProducts() {
        Iterable<Product> listProduct = Arrays.asList(product2);
        lenient().when(stockManagementService.getOutOfStockProducts()).thenReturn((List<Product>) listProduct);
        Iterable<Product> listProductResult = stockManagementService.getOutOfStockProducts();
        Assertions.assertIterableEquals(listProduct, listProductResult);
    }

    @Test
    void testGetLowStockProducts() {
        Iterable<Product> listProduct = Arrays.asList(product2, product3);
        lenient().when(stockManagementService.getLowStockProducts()).thenReturn((List<Product>) listProduct);
        Iterable<Product> listProductResult = stockManagementService.getLowStockProducts();
        Assertions.assertIterableEquals(listProduct, listProductResult);
    }

    @Test
    void testUpdateStock() {
        product1.setStock(50);
        when(productRepository.findById(product1.getId())).thenReturn(product1);
        lenient().when(stockManagementService.updateStock(product1.getId(), product1.getStock())).thenReturn(product1);
        Product updatedStock = stockManagementService.updateStock(product1.getId(), 50);

        Assertions.assertEquals(product1.getStock(), updatedStock.getStock());

        // Test negative value
        when(productRepository.findById(product2.getId())).thenReturn(product2);
        lenient().when(stockManagementService.updateStock(product2.getId(), product2.getStock())).thenReturn(product2);
        updatedStock = stockManagementService.updateStock(product2.getId(), -10);

        Assertions.assertEquals(product2.getStock(), updatedStock.getStock());
        Assertions.assertNotEquals(-10, updatedStock.getStock());
    }

    @Test
    void testBulkUpdateStock() {
        product2.setStock(50);
        product3.setStock(75);
        Set keys = new HashSet();
        keys.add(product2.getId());
        keys.add(product3.getId());
        when(productRepository.findAllById(keys)).thenReturn(Arrays.asList(product2, product3));
        Iterable<Product> listProduct = Arrays.asList(product2, product3);

        HashMap<Integer, Integer> newStock = new HashMap<Integer, Integer>();
        newStock.put(product2.getId(), 50);
        newStock.put(product3.getId(), 75);
        lenient().when(stockManagementService.bulkUpdateStock(newStock)).thenReturn((List<Product>) listProduct);

        Iterable<Product> updatedStock = stockManagementService.bulkUpdateStock(newStock);
        Assertions.assertIterableEquals(listProduct, updatedStock);

        // Test negative value
        keys = new HashSet();
        keys.add(product1.getId());
        when(productRepository.findAllById(keys)).thenReturn(Arrays.asList(product1));
        listProduct = Arrays.asList(product1);

        newStock = new HashMap<Integer, Integer>();
        newStock.put(product1.getId(), -10);
        lenient().when(stockManagementService.bulkUpdateStock(newStock)).thenReturn((List<Product>) listProduct);

        updatedStock = stockManagementService.bulkUpdateStock(newStock);
        Assertions.assertIterableEquals(listProduct, updatedStock);
    }
}
