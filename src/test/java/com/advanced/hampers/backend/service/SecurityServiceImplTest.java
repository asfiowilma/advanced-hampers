package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.User;
import com.advanced.hampers.backend.repository.UserRepository;
import com.advanced.hampers.backend.security.HampersUserDetails;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
class SecurityServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private Authentication auth;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    private SecurityServiceImpl securityService;
    @InjectMocks
    private HampersUserDetailsService userDetailsService;

    private User user;
    private HampersUserDetails userDetails;

    @BeforeEach
    public void setUp() {
        user = new User("user", "password", "USER");
        userDetails = new HampersUserDetails(user);
    }

    @Test
    void findLoggedInUserDetailsNotLoggedIn() throws Exception {
        lenient().when(auth.getPrincipal()).thenReturn("mock");
        SecurityContextHolder.getContext().setAuthentication(auth);
        assertNull(securityService.findLoggedInUserDetails());
    }

    @Test
    void findLoggedInUserDetails() throws Exception {
        when(auth.getPrincipal()).thenReturn(userDetails);
        SecurityContextHolder.getContext().setAuthentication(auth);
        assertEquals(userDetails, securityService.findLoggedInUserDetails());
    }
}