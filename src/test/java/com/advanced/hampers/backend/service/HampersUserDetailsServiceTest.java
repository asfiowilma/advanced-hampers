package com.advanced.hampers.backend.service;

import com.advanced.hampers.backend.model.User;
import com.advanced.hampers.backend.repository.UserRepository;
import com.advanced.hampers.backend.security.HampersUserDetails;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HampersUserDetailsServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private HampersUserDetailsService userDetailsService;

    private User user;
    private HampersUserDetails userDetails;

    @BeforeEach
    public void setUp() {
        user = new User("username", "password", "USER");
        user.setPasswordConfirm("password");
        userDetails = new HampersUserDetails(user);
    }

    @Test
    void loadUserByUsername() throws Exception {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));
        UserDetails loaded = userDetailsService.loadUserByUsername(user.getUsername());
        assertEquals(loaded.getUsername(), userDetails.getUsername());
        assertEquals(loaded.getPassword(), userDetails.getPassword());
    }

    @Test
    void loadUserByUsernameNotFound() throws Exception {
        when(userRepository.findByUsername(user.getUsername()+"a")).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(user.getUsername()+"a"));
    }

    @Test
    void userDetailsMethods() throws Exception {
        HampersUserDetails ud = new HampersUserDetails();
        assertNull(ud.getAuthorities());
        assertNull(ud.getUsername());
        assertNull(ud.getPassword());
        assertTrue(ud.isAccountNonExpired());
        assertTrue(ud.isAccountNonLocked());
        assertTrue(ud.isCredentialsNonExpired());
        assertFalse(ud.isEnabled());
    }

}