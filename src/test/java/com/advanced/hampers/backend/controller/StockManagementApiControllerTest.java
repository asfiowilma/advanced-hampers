package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.ProductServiceImpl;
import com.advanced.hampers.backend.service.StockManagementServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = StockManagementApiController.class)
public class StockManagementApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductServiceImpl productService;

    @MockBean
    private HampersUserDetailsService userDetailsService;

    @MockBean
    private StockManagementServiceImpl stockManagementService;

    private Product product1;
    private Product product2;
    private Product product3;

    @BeforeEach
    public void setUp(){
        product1 = new Product(1, "batu", "https://images.jpg");
        product1.setStock(20);
        product2 = new Product(5, "bengbeng", "https://bengbeng.jpg");
        product2.setStock(0);
        product3 = new Product(15, "permen", "https://permen.jpg");
        product3.setStock(5);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetAvailableProducts() throws Exception {
        Iterable<Product> listProduct = Arrays.asList(product1, product3);
        when(stockManagementService.getAvailableProducts()).thenReturn((List<Product>) listProduct);

        mvc.perform(get("/api/stock-management/available/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(product1.getId()))
                .andExpect(jsonPath("$[1].id").value(product3.getId()));;
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetOutOfStockProducts() throws Exception {
        Iterable<Product> listProduct = Arrays.asList(product2);
        when(stockManagementService.getOutOfStockProducts()).thenReturn((List<Product>) listProduct);

        mvc.perform(get("/api/stock-management/outofstock/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(product2.getId()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetLowStockProduct() throws Exception {
        Iterable<Product> listProduct = Arrays.asList(product2, product3);
        when(stockManagementService.getLowStockProducts()).thenReturn((List<Product>) listProduct);

        mvc.perform(get("/api/stock-management/low/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(product2.getId()))
                .andExpect(jsonPath("$[1].id").value(product3.getId()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testUpdateStock() throws Exception {
        product1.setStock(50);
        when(stockManagementService.updateStock(eq(product1.getId()), eq(50))).thenReturn(product1);

        mvc.perform(put("/api/stock-management/" + product1.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"jumlah\":50}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.stock").value(50));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testBulkUpdateStock() throws Exception {
        product2.setStock(50);
        product3.setStock(75);
        Iterable<Product> listProduct = Arrays.asList(product2, product3);
        HashMap<Integer, Integer> newStock = new HashMap<Integer, Integer>();
        newStock.put(product2.getId(), 50);
        newStock.put(product3.getId(), 75);
        when(stockManagementService.bulkUpdateStock(newStock)).thenReturn((List<Product>) listProduct);

        mvc.perform(put("/api/stock-management/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("[\n" +
                        "    {\n" +
                        "        \"id\": " + product2.getId() + ",\n" +
                        "        \"jumlah\": 50\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"id\": " + product3.getId() + ",\n" +
                        "        \"jumlah\": 75\n" +
                        "    }\n" +
                        "]"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].stock").value(50))
                .andExpect(jsonPath("$[1].stock").value(75));
    }
}