package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.*;
import com.advanced.hampers.backend.repository.CheckoutRepository;
import com.advanced.hampers.backend.service.CheckoutServiceImpl;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.annotations.Check;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CheckoutApiController.class)
public class CheckoutApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CheckoutServiceImpl checkoutService;

    @MockBean
    private HampersUserDetailsService userDetailsService;


    private Checkout checkout1;
    private PackagingModel pg;
    private ProductSummary ps;
    private User user;
    private Product p;


    @BeforeEach
    public void setUp() {
        p =  new Product(3,"Brownies","https://images.unsplash.com/photo-1592177538809-f713fd7d82c9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80");
        user = new User("logintest","logintest","ADMIN, USER");
        List<ProductSummary> productId = new ArrayList<>();
        List<Decoration> dc = new ArrayList<>();
        dc.add(new Decoration("Pita",pg));
        ps = new ProductSummary(p,2);
        productId.add(ps);
        pg = new PackagingModel("Besar",60,dc);
        checkout1 = new Checkout(user,pg,"2020/03/12 12:12", productId,"Menunggu Konfirmasi");

    }
    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testCreateSummary() throws Exception {
        when(checkoutService.createSummary(checkout1)).thenReturn(checkout1);
        mvc.perform(post("/api/checkout/")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(checkout1)));
//                .andExpect(jsonPath("$[0].user.id").value(checkout1.getUser().getId()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetProductIdByOrderId() throws Exception {
        Set<Integer> productId = new HashSet<>();
        for(ProductSummary i : checkout1.getProductSummaries()){
            productId.add(i.getProduct().getId());
        }
        when(checkoutService.createSummary(checkout1)).thenReturn(checkout1);
        when(checkoutService.getProductIdByOrderId(checkout1.getOrderId())).thenReturn(productId);

        mvc.perform(get("/api/checkout/" + checkout1.getOrderId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(content()
//                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].user.id").value(checkout1.getUser().getId()));
//        mvc.perform(get("/api/checkout/322121")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetDecorationsByOrderId() throws Exception {
        Set<Integer> productId = new HashSet<>();
        for(ProductSummary i : checkout1.getProductSummaries()){
            productId.add(i.getProduct().getId());
        }
        when(checkoutService.createSummary(checkout1)).thenReturn(checkout1);
        when(checkoutService.getProductIdByOrderId(checkout1.getOrderId())).thenReturn(productId);

        mvc.perform(get("/api/checkout/d/" + checkout1.getOrderId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
//                .andExpect(jsonPath("$.packagingId").value(checkout1.getPackaging().getPackagingId()));
        mvc.perform(get("/api/packaging/d/1234")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetAllSummary() throws Exception {
        Iterable<Checkout> listSummary = Arrays.asList(checkout1);
        when(checkoutService.getAllSummary()).thenReturn((List<Checkout>) listSummary);

        mvc.perform(get("/api/checkout/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].packaging.packagingId").value(checkout1.getOrderId()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testGetSummaryById() throws Exception {
        when(checkoutService.getSummaryById(checkout1.getOrderId())).thenReturn(checkout1);

        mvc.perform(get("/api/checkout/" + checkout1.getOrderId() + "/summary").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.packaging.packagingId").value(checkout1.getOrderId()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void testDeleteSummary() throws Exception {
        mvc.perform(delete("/api/checkout/" + checkout1.getOrderId()))
                .andExpect(status().isNoContent());
    }
}
