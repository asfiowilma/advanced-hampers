package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Decoration;
import com.advanced.hampers.backend.model.PackagingModel;
import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.PackagingServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PackagingApiController.class)
public class PackagingApiControllerTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private PackagingServiceImpl packagingService;
    @MockBean
    private HampersUserDetailsService userDetailsService;

    private PackagingModel packaging;

    @BeforeEach
    public void setUp(){
        List<Decoration> listdecorations = new ArrayList<>();
        listdecorations.add(new Decoration());
        packaging = new PackagingModel("small", 15, listdecorations);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getAllPackagings() throws Exception {
        List<PackagingModel> listPackaging = Collections.singletonList(packaging);
        when(packagingService.getListPackaging()).thenReturn(listPackaging);

        mvc.perform(get("/api/packaging/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].packagingId").value(packaging.getPackagingId()));
    }


    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getPackagingById() throws Exception {
        when(packagingService.getPackagingById(packaging.getPackagingId())).thenReturn(packaging);

        mvc.perform(get("/api/packaging/" + packaging.getPackagingId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.packagingId").value(packaging.getPackagingId()));
        mvc.perform(get("/api/packaging/1234")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void createPackaging() throws Exception {
        when(packagingService.createPackaging(packaging)).thenReturn(packaging);
        mvc.perform(post("/api/packaging/")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(packaging)))
                .andExpect(jsonPath("$.jenisPackage").value("small"));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void updatePackaging() throws Exception {
        when(packagingService.updatePackaging(eq(packaging.getPackagingId()), any(PackagingModel.class))).thenReturn(packaging);

        mvc.perform(put("/api/packaging/" + packaging.getPackagingId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(packaging)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.packagingId").value(packaging.getPackagingId()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void deletePackaging() throws Exception {
        mvc.perform(delete("/api/packaging/" + packaging.getPackagingId()))
                .andExpect(status().isNoContent());
    }
}
