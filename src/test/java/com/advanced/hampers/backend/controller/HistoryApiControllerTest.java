package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.*;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.HistoryServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = HistoryApiController.class)
public class HistoryApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private HistoryServiceImpl historyService;

    @MockBean
    private HampersUserDetailsService userDetailsService;

    private Checkout checkout;
    private PackagingModel pg;
    private ProductSummary ps;
    private Product p;
    private User user;

    @BeforeEach
    public void setUp() {
        p =  new Product(3,"Brownies","https://images.unsplash.com/photo-1592177538809-f713fd7d82c9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80");
        user = new User("logintest","logintest","ADMIN, USER");
        List<ProductSummary> productId = new ArrayList<>();
        List<Decoration> dc = new ArrayList<>();
        dc.add(new Decoration("Pita",pg));
        ps = new ProductSummary(p,2);
        productId.add(ps);
        pg = new PackagingModel("Besar",60,dc);
        checkout = new Checkout(user,pg,"2020/03/12 12:12", productId,"Menunggu Konfirmasi");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void TestGetAllHistory() throws Exception {
        List<Checkout> listOrders = Collections.singletonList(checkout);
        when(historyService.getAllOrder()).thenReturn(listOrders);

        mvc.perform(get("/api/history/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].status").value("Menunggu Konfirmasi"));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getOrderById() throws Exception {
        when(historyService.getOrderById(checkout.getOrderId())).thenReturn(checkout);

        mvc.perform(get("/api/history/" + checkout.getOrderId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("Menunggu Konfirmasi"));
        mvc.perform(get("/api/history/78")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getOrderByUsername() throws Exception {
        List<Checkout> listOrders = Collections.singletonList(checkout);
        when(historyService.getOrderByUsername(checkout.getUser().getUsername())).thenReturn(listOrders);

        mvc.perform(get("/api/history/u/" + checkout.getUser().getUsername())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void TestBestSellers() throws Exception {
        mvc.perform(get("/api/history/bestsellers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}
