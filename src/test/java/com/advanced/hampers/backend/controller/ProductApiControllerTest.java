package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.Product;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.ProductServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ProductApiController.class)
public class ProductApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductServiceImpl productService;
    @MockBean
    private HampersUserDetailsService userDetailsService;

    private Product product;

    @BeforeEach
    public void setUp(){
        product = new Product(1, "batu", "https://images.jpg");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getAllProducts() throws Exception {
        List<Product> listProduct = Collections.singletonList(product);
        when(productService.getAllProduct()).thenReturn(listProduct);

        mvc.perform(get("/api/product/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(product.getId()));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getProductById() throws Exception {
        when(productService.getProductById(product.getId())).thenReturn(product);

        mvc.perform(get("/api/product/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(product.getId()));
        mvc.perform(get("/api/product/234")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getProductByName() throws Exception {
        when(productService.getProductByName(product.getName())).thenReturn(product);

        mvc.perform(get("/api/product/n/" + product.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(product.getId()));
        mvc.perform(get("/api/product/n/wowo")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void createProduct() throws Exception {
        when(productService.createProduct(product)).thenReturn(product);
        mvc.perform(post("/api/product/")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(product)))
                .andExpect(jsonPath("$.name").value("batu"));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void updateProduct() throws Exception {
        when(productService.updateProduct(eq(product.getId()), any(Product.class))).thenReturn(product);

        mvc.perform(put("/api/product/" + product.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(product)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(product.getId()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void deleteProduct() throws Exception {
        mvc.perform(delete("/api/product/" + product.getId()))
                .andExpect(status().isNoContent());
    }
}