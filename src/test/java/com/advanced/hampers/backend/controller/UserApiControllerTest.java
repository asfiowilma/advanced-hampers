package com.advanced.hampers.backend.controller;

import com.advanced.hampers.backend.model.User;
import com.advanced.hampers.backend.security.HampersUserDetails;
import com.advanced.hampers.backend.security.UserValidator;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.SecurityServiceImpl;
import com.advanced.hampers.backend.service.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserApiController.class)
@Import(UserValidator.class)
class UserApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserServiceImpl userService;
    @MockBean
    private SecurityServiceImpl securityService;
    @MockBean
    private HampersUserDetailsService userDetailsService;

    private User user;
    private HampersUserDetails userDetails;

    @BeforeEach
    public void setUp() {
        user = new User("valentino", "password", "USER");
        user.setPasswordConfirm("password");
        userDetails = new HampersUserDetails(user);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getLoggedInUserDetails() throws Exception {
        when(securityService.findLoggedInUserDetails()).thenReturn(userDetails);
        when(userService.findByUsername(user.getUsername())).thenReturn(user);

        mvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username").value(user.getUsername()));
    }

    @Test
    void register() throws Exception {
        user.setUsername("bagusbagus");
        when(userService.findByUsername(user.getUsername())).thenThrow(new UsernameNotFoundException(""));
        mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", user.getUsername())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm())
                .sessionAttr("user", new User()))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void badRegisterShortPassword() throws Exception {
        user.setUsername("azkafitria");
        user.setPassword("abc");
        user.setPasswordConfirm("abc");

        when(userService.register(user)).thenReturn(user);
        mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", user.getUsername())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm())
                .sessionAttr("user", new User()))
                .andExpect(view().name("auth/registration"));

    }

    @Test
    void badRegisterShortUsername() throws Exception {
        user.setUsername("bagus");

        when(userService.register(user)).thenReturn(user);
        mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", user.getUsername())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm())
                .sessionAttr("user", new User()))
                .andExpect(view().name("auth/registration"));

    }

    @Test
    void badRegisterLongUsername() throws Exception {
        user.setUsername("123456781234567812345678123456789");

        when(userService.register(user)).thenReturn(user);
        mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", user.getUsername())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm())
                .sessionAttr("user", new User()))
                .andExpect(view().name("auth/registration"));

    }

    @Test
    void badRegisterLongPassword() throws Exception {
        user.setPassword("123456781234567812345678123456789");
        user.setPasswordConfirm("123456781234567812345678123456789");

        when(userService.register(user)).thenReturn(user);
        mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", user.getUsername())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm())
                .sessionAttr("user", new User()))
                .andExpect(view().name("auth/registration"));

    }

    @Test
    void badRegisterWrongPassword() throws Exception {
        user.setPasswordConfirm("abc");

        when(userService.register(user)).thenReturn(user);
        mvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", user.getUsername())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm())
                .sessionAttr("user", new User()))
                .andExpect(view().name("auth/registration"));

    }

    @Test
    void validatorSupports() throws Exception {
        UserValidator userValidator = new UserValidator();
        assertTrue(userValidator.supports(User.class));
    }
}