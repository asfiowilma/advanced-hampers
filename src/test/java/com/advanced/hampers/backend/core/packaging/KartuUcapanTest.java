package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class KartuUcapanTest {

    private Class<?> kartuUcapanClass;

    @BeforeEach
    public void setUp() throws Exception{
        kartuUcapanClass = Class.forName("com.advanced.hampers.backend.core.packaging.Stiker");
    }

    @Test
    public void testKartuUcapanIsConcrete() throws Exception{
        assertFalse(Modifier.isAbstract(kartuUcapanClass.getModifiers()));
    }

    @Test
    public void testKartuUcapanIsAPackagingDecorator() throws Exception{
        Class<?> parentClass = kartuUcapanClass.getSuperclass();

        assertEquals("com.advanced.hampers.backend.core.packaging.PackagingDecorator", parentClass.getName());
    }

    @Test
    public void testGetDescriptionMethodReturnString() throws Exception{
        Method getDescription = kartuUcapanClass.getMethod("getDescription");

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetCapacityMethodReturnInt() throws Exception{
        Method getCapacity = kartuUcapanClass.getMethod("getCapacity");

        assertEquals("int", getCapacity.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDecorationsMethodReturningStringList() throws Exception{
        Method getDecorations = kartuUcapanClass.getMethod("getDecorations");

        assertEquals("java.util.List<java.lang.String>", getDecorations.getGenericReturnType().getTypeName());
    }

}
