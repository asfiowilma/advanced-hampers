package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class StikerTest {

    private Class<?> stikerClass;

    @BeforeEach
    public void setUp() throws Exception{
        stikerClass = Class.forName("com.advanced.hampers.backend.core.packaging.Stiker");
    }

    @Test
    public void testStikerIsConcrete() throws Exception{
        assertFalse(Modifier.isAbstract(stikerClass.getModifiers()));
    }

    @Test
    public void testStikerIsAPackagingDecorator() throws Exception{
        Class<?> parentClass = stikerClass.getSuperclass();

        assertEquals("com.advanced.hampers.backend.core.packaging.PackagingDecorator", parentClass.getName());
    }

    @Test
    public void testGetDescriptionMethodReturnString() throws Exception{
        Method getDescription = stikerClass.getMethod("getDescription");

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetCapacityMethodReturnInt() throws Exception{
        Method getCapacity = stikerClass.getMethod("getCapacity");

        assertEquals("int", getCapacity.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDecorationsMethodReturningStringList() throws Exception{
        Method getDecorations = stikerClass.getMethod("getDecorations");

        assertEquals("java.util.List<java.lang.String>", getDecorations.getGenericReturnType().getTypeName());
    }

}
