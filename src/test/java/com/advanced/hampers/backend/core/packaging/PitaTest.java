package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class PitaTest {

    private Class<?> pitaClass;

    @BeforeEach
    public void setUp() throws Exception{
        pitaClass = Class.forName("com.advanced.hampers.backend.core.packaging.Pita");
    }

    @Test
    public void testPitaIsConcrete() throws Exception{
        assertFalse(Modifier.isAbstract(pitaClass.getModifiers()));
    }

    @Test
    public void testPitaIsAPackagingDecorator() throws Exception{
        Class<?> parentClass = pitaClass.getSuperclass();

        assertEquals("com.advanced.hampers.backend.core.packaging.PackagingDecorator", parentClass.getName());
    }

    @Test
    public void testGetDescriptionMethodReturnString() throws Exception{
        Method getDescription = pitaClass.getMethod("getDescription");

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetCapacityMethodReturnInt() throws Exception{
        Method getCapacity = pitaClass.getMethod("getCapacity");

        assertEquals("int", getCapacity.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDecorationsMethodReturningStringList() throws Exception{
        Method getDecorations = pitaClass.getMethod("getDecorations");

        assertEquals("java.util.List<java.lang.String>", getDecorations.getGenericReturnType().getTypeName());
    }
}
