package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MediumPackagingTest {

    private Class<?> mediumPackagingClass;

    @BeforeEach
    public void setUp() throws Exception{
        mediumPackagingClass = Class.forName("com.advanced.hampers.backend.core.packaging.MediumPackaging");
    }

    @Test
    public void testMediumPackagingIsConcrete() throws Exception{
        assertFalse(Modifier.isAbstract(mediumPackagingClass.getModifiers()));
    }

    @Test
    public void testMediumPackagingIsAPackaging() throws Exception{
        Class<?> parentClass = mediumPackagingClass.getSuperclass();

        assertEquals("com.advanced.hampers.backend.core.packaging.Packaging",
                parentClass.getName());
    }

    @Test
    public void testGetDecorationsMethodReturningStringList() throws Exception{
        Method getDecorations = mediumPackagingClass.getMethod("getDecorations");

        assertFalse(Modifier.isAbstract(getDecorations.getModifiers()));
        assertEquals("java.util.List<java.lang.String>", getDecorations.getGenericReturnType().getTypeName());
    }

}
