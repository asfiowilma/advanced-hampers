package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SmallPackagingTest {

    private Class<?> smallPackagingClass;

    @BeforeEach
    public void setUp() throws Exception{
        smallPackagingClass = Class.forName("com.advanced.hampers.backend.core.packaging.SmallPackaging");
    }

    @Test
    public void testSmallPackagingIsConcrete() throws Exception{
        assertFalse(Modifier.isAbstract(smallPackagingClass.getModifiers()));
    }

    @Test
    public void testSmallPackagingIsAPackaging() throws Exception{
        Class<?> parentClass = smallPackagingClass.getSuperclass();

        assertEquals("com.advanced.hampers.backend.core.packaging.Packaging",
                parentClass.getName());
    }

    @Test
    public void testGetDecorationsMethodReturningStringList() throws Exception{
        Method getDecorations = smallPackagingClass.getMethod("getDecorations");

        assertFalse(Modifier.isAbstract(getDecorations.getModifiers()));
        assertEquals("java.util.List<java.lang.String>", getDecorations.getGenericReturnType().getTypeName());
    }
}
