package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class LargePackagingTest {

    private Class<?> largePackagingClass;

    @BeforeEach
    public void setUp() throws Exception{
        largePackagingClass = Class.forName("com.advanced.hampers.backend.core.packaging.LargePackaging");
    }

    @Test
    public void testLargePackagingIsConcrete() throws Exception{
        assertFalse(Modifier.isAbstract(largePackagingClass.getModifiers()));
    }

    @Test
    public void testLargePackagingIsAPackaging() throws Exception{
        Class<?> parentClass = largePackagingClass.getSuperclass();

        assertEquals("com.advanced.hampers.backend.core.packaging.Packaging",
                parentClass.getName());
    }

    @Test
    public void testGetDecorationsMethodReturningStringList() throws Exception{
        Method getDecorations = largePackagingClass.getMethod("getDecorations");

        assertFalse(Modifier.isAbstract(getDecorations.getModifiers()));
        assertEquals("java.util.List<java.lang.String>", getDecorations.getGenericReturnType().getTypeName());
    }

}
