package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PackagingTest {

    private Class<?> packagingClass;

    @BeforeEach
    public void setUp() throws Exception{
        packagingClass = Class.forName("com.advanced.hampers.backend.core.packaging.Packaging");
    }

    @Test
    public void testPackagingIsAbstract() throws Exception{
        assertTrue(Modifier.isAbstract(packagingClass.getModifiers()));
    }

    @Test
    public void testPackagingHasGetDescriptionMethod() throws Exception{
        Method description = packagingClass.getDeclaredMethod("getDescription");

        assertEquals(0, description.getParameterCount());
        assertEquals("java.lang.String", description.getGenericReturnType().getTypeName());
        assertTrue(Modifier.isPublic(description.getModifiers()));

    }

    @Test
    public void testPackagingHasGetCapacityMethod() throws Exception{
        Method getCapacity = packagingClass.getDeclaredMethod("getCapacity");

        assertEquals(0, getCapacity.getParameterCount());
        assertEquals("int", getCapacity.getGenericReturnType().getTypeName());
        assertTrue(Modifier.isPublic(getCapacity.getModifiers()));
    }

    @Test
    public void testPackagingHasAbstractGetDecorationsMethod() throws Exception{

        Method decorations = packagingClass.getDeclaredMethod("getDecorations");
        assertTrue(Modifier.isAbstract(decorations.getModifiers()));
    }

}
