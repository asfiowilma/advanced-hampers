package com.advanced.hampers.backend.core.packaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PackagingDecoratorTest {

    private Class<?> packagingDecoratorClass;

    @BeforeEach
    public void setUp() throws Exception{
        packagingDecoratorClass = Class.forName("com.advanced.hampers.backend.core.packaging.PackagingDecorator");
    }

    @Test
    public void testPackagingDecoratorIsAbtract() throws Exception{
        assertTrue(Modifier.isAbstract(packagingDecoratorClass.getModifiers()));
    }

    @Test
    public void testPackagingDecoratorHasAbstractGetDescriptionMethod() throws Exception{
        Method getDescription = packagingDecoratorClass.getMethod("getDescription");

        assertTrue(Modifier.isAbstract(getDescription.getModifiers()));
    }

    @Test
    public void testPackagingDecoratorHasAbstractGetCapacityMethod() throws Exception{
        Method getCapacity = packagingDecoratorClass.getMethod("getCapacity");

        assertTrue(Modifier.isAbstract(getCapacity.getModifiers()));
    }
}
