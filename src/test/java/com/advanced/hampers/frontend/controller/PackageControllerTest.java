package com.advanced.hampers.frontend.controller;

import com.advanced.hampers.backend.controller.PackageController;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.PackagingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PackageController.class)
public class PackageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HampersUserDetailsService hampersUserDetailsService;

    @MockBean
    private PackagingService packagingService;

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void urlPackageSelectionTest() throws Exception{
        mockMvc.perform(get("/package/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("choosePackage"))
                .andExpect(view().name("package/packageSelection"));
    }

}
