package com.advanced.hampers.frontend.controller;

import com.advanced.hampers.backend.service.HampersUserDetailsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = HistoryController.class)
public class HistoryApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HampersUserDetailsService userDetailsService;

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void getAllHistoryTest() throws Exception {
        mockMvc.perform(get("/history"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getAllHistory"))
                .andExpect(view().name("history/history"));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    void getOrderByIdTest() throws Exception {
        mockMvc.perform(get("/history/1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getOrderById"))
                .andExpect(view().name("history/details"));
    }
}
