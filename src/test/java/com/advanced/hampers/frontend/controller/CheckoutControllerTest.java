package com.advanced.hampers.frontend.controller;

import com.advanced.hampers.backend.service.HampersUserDetailsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CheckoutController.class)
public class CheckoutControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HampersUserDetailsService userDetailsService;

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void URLcheckoutSummaryTest() throws Exception {
        mockMvc.perform(get("/checkout/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("checkoutSummary"))
                .andExpect(view().name("checkout/summary"));
    }
}
