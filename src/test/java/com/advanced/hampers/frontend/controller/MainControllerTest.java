package com.advanced.hampers.frontend.controller;

import com.advanced.hampers.backend.service.HampersUserDetailsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MainController.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HampersUserDetailsService userDetailsService;

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void homeTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("home"))
                .andExpect(view().name("home"));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void buildTest() throws Exception {
        mockMvc.perform(get("/build"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("build"))
                .andExpect(view().name("build"));
    }
}
