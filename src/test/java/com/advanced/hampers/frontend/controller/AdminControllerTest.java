package com.advanced.hampers.frontend.controller;

import com.advanced.hampers.backend.service.CheckoutService;
import com.advanced.hampers.backend.service.HampersUserDetailsService;
import com.advanced.hampers.backend.service.ProductService;
import com.advanced.hampers.backend.service.StockManagementService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AdminController.class)
public class AdminControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private StockManagementService stockManagementService;

    @MockBean
    private HampersUserDetailsService userDetailsService;

    @MockBean
    private CheckoutService checkoutService;

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void adminGetTest() throws Exception {
        mvc.perform(get("/admin"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("admin"))
                .andExpect(view().name("admin/admin"));
    }

    @Test
    @WithMockUser(authorities = {"ADMIN", "USER"})
    public void confirmGetTest() throws Exception {
        mvc.perform(get("/admin/confirm"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("confirm"))
                .andExpect(view().name("admin/confirm"));
    }
}
